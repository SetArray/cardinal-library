package cardinal.componentArch.events;

import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Event;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TransformEvent extends Event
{	
	public TransformEvent(Vector3f position, Vector3f rotation, boolean translatePrevTransform)
	{
		super(getStaticID());
		
		addParamater("position", position);
		addParamater("rotation", rotation);
		addParamater("translate", translatePrevTransform);
	}

	public static String getStaticID()
	{
		return "TransformEvent";
	}
}
