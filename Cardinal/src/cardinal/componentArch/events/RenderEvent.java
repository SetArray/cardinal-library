package cardinal.componentArch.events;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Event;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RenderEvent extends Event
{
	public RenderEvent()
	{
		super(getStaticID());
		
		//addParamater("", null);
	}
	
	public static String getStaticID()
	{
		return "RenderEvent";
	}
}
