package cardinal.componentArch;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public abstract class Component
{
	protected GameObject parentObject;
	
	public Component()
	{
		awake();
	}
	
	/**
	 * Updates once per frame
	 * @param object
	 */
	public abstract void update();
	
	/**
	 * Updates once per frame after initial update
	 * @param object
	 */
	public abstract void lateUpdate();
	
	/**
	 * Means of communication to Components
	 * @param event
	 * @return
	 */
	public abstract boolean fireEvent(Event event);
	
	/**
	 * Called upon initialization
	 */
	public abstract void awake();
	
	/**
	 * Called when the Component is assigned to a GameObject
	 */
	public abstract void start();
	
	/**
	 * Clear memory before deleting object
	 */
	public abstract void destroy();

	/**
	 * @return ID for identifying the type of Component
	 */
	public abstract String getID();
	
	public void setParentObject(GameObject parent)
	{
		this.parentObject = parent;
	}
	
	public GameObject getParentObject()
	{
		return parentObject;
	}
}