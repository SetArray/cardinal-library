package cardinal.componentArch;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public enum ComponentType
{
	TRANSFORM, 
	TEXTURED_MODEL, 
	HEIGHTMAP_MODEL, 
	TERRAIN_COLLIDER, 
	RENDER,
	RENDER_PROPERTIES, 
	TEXT
}
