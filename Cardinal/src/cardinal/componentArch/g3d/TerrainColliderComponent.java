package cardinal.componentArch.g3d;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Component;
import cardinal.componentArch.Event;
import cardinal.graphics.model.g3d.HeightmapModel;
import cardinal.util.VectorUtil;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TerrainColliderComponent extends Component
{
	private TransformComponent transformComponent;
	private HeightmapModel heightmap;
	private float[][] heightData;
	private Vector2f scale;
	
	public TerrainColliderComponent(TransformComponent transformComponent, HeightmapModel heightmap)
	{
		this.transformComponent = transformComponent;
		this.heightmap = heightmap;
		this.heightData = heightmap.getHeightData();
		this.scale = heightmap.getScale();
	}

	@Override
	public void update() 
	{
		transformComponent.getPosition3f().y = (heightmap.getHeightAtWorldPosition(transformComponent.getPosition3f()));
	}
	
	@Override
	public boolean fireEvent(Event event)
	{
		return false;
	}

	@Override
	public void lateUpdate() {}

	@Override
	public void awake() {}

	@Override
	public void start() {}

	@Override
	public void destroy() {}
	

	@Override
	public String getID()
	{
		return getStaticID();
	}
	
	public static String getStaticID()
	{
		return "TerrainColliderComponent";
	}
}
