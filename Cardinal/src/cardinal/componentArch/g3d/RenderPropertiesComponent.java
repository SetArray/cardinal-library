package cardinal.componentArch.g3d;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Component;
import cardinal.componentArch.Event;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RenderPropertiesComponent extends Component
{
	private boolean is3D;
	private boolean clampTextureOnEdges;
	private boolean isMultiTextured;
	
	private boolean applyAmbientLight;
	private boolean applyDiffuseLight;
	private boolean applySpecularLight;

	public RenderPropertiesComponent(boolean is3D)
	{
		this.is3D = is3D;
	}
	
	@Override
	public void update() {}
	
	@Override
	public boolean fireEvent(Event event)
	{
		return false;
	}

	@Override
	public void lateUpdate() {}

	@Override
	public void awake() {}

	@Override
	public void start() {}

	@Override
	public void destroy() {}

	@Override
	public String getID()
	{
		return getStaticID();
	}

	public static String getStaticID()
	{
		return "RenderPropertiesComponent";
	}

	public void set3D(boolean shouldRender3D)
	{
		this.is3D = shouldRender3D;
	}

	public void setClampTextureOnEdges(boolean clampTextureOnEdges)
	{
		this.clampTextureOnEdges = clampTextureOnEdges;
	}
	
	public void setIsMultiTextured(boolean isMultiTextured)
	{
		this.isMultiTextured = isMultiTextured;
	}

	public void setApplyAmbientLight(boolean applyAmbientLight)
	{
		this.applyAmbientLight = applyAmbientLight;
	}

	public void setApplyDiffuseLight(boolean applyDiffuseLight)
	{
		this.applyDiffuseLight = applyDiffuseLight;
	}

	public void applySpecularLight(boolean applySpecularLight)
	{
		this.applySpecularLight = applySpecularLight;
	}

	public boolean is3D()
	{
		return is3D;
	}

	public boolean clampTextureOnEdges()
	{
		return clampTextureOnEdges;
	}
	
	public boolean isMultiTextured()
	{
		return isMultiTextured;
	}

	public boolean applyAmbientLight()
	{
		return applyAmbientLight;
	}

	public boolean applyDiffuseLight()
	{
		return applyDiffuseLight;
	}

	public boolean aplySpecularLight()
	{
		return applySpecularLight;
	}
}
