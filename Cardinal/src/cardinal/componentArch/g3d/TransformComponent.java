package cardinal.componentArch.g3d;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Component;
import cardinal.componentArch.Event;
import cardinal.componentArch.events.TransformEvent;
import cardinal.util.VectorUtil;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TransformComponent extends Component
{
	private Matrix4f modelMatrix;
	private Vector3f position;
	private Vector3f rotation;
	private Vector3f scale;

	private Vector3f rotationMinima;
	private Vector3f rotationMaxima;
	private boolean restrictRotation;

	public TransformComponent()
	{
		this(new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f));
	}

	public TransformComponent(Vector2f position)
	{
		this(new Vector3f(position.x, position.y, 0.0f));
	}
	
	public TransformComponent(Vector3f position)
	{
		this(position, new Vector3f(0.0f, 0.0f, 0.0f));
	}

	public TransformComponent(Vector3f position, Vector3f rotation)
	{
		this.position = position;
		this.rotation = rotation;
		this.scale = new Vector3f(1.0f, 1.0f, 1.0f);

		modelMatrix = new Matrix4f();
		setModelMatrix();
	}

	public void setModelMatrix()
	{
		modelMatrix.setIdentity();
		Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1, 0, 0), modelMatrix, modelMatrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0, 1, 0), modelMatrix, modelMatrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0, 0, 1), modelMatrix, modelMatrix);
		Matrix4f.translate(new Vector3f(position.x, position.y, -position.z), modelMatrix, modelMatrix);
		Matrix4f.scale(new Vector3f(1.0f, 1.0f, -1.0f), modelMatrix, modelMatrix);
	}
	
	public Matrix4f getModelMatrix()
	{
		return modelMatrix;
	}

	@Override
	public void awake()
	{
	}

	@Override
	public void start() {}


	@Override
	public void update()
	{
		VectorUtil.cycleVector(rotation, 0.0f, 360.0f, false);
		
		if(restrictRotation)
			VectorUtil.limitVector(rotation, rotationMinima, rotationMaxima);
	}
	
	@Override
	public boolean fireEvent(Event event)
	{
		if(event instanceof TransformEvent)
		{
			Vector3f paramPosition = (Vector3f) event.getParamater("position");
			Vector3f paramRotation = (Vector3f) event.getParamater("rotation");
			
			if((boolean) event.getParamater("translate"))
			{
				position.x += paramPosition.x;
				position.y += paramPosition.y;
				position.z += paramPosition.z;
				
				rotation.x += paramRotation.x;
				rotation.y += paramRotation.y;
				rotation.z += paramRotation.z;
			}
			else
			{
				position = paramPosition;
				rotation = paramRotation;
			}
			
			return true;
		}
		return false;
	}

	@Override
	public void lateUpdate()
	{
		setModelMatrix();
	}

	@Override
	public void destroy() {}


	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public Vector3f getPosition3f()
	{
		return position;
	}

	public Vector2f getPosition2f()
	{
		return new Vector2f(position.x, position.y);
	}

	public float getX()
	{
		return position.x;
	}

	public float getY()
	{
		return position.y;
	}

	public float getZ()
	{
		return position.z;
	}

	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}

	public Vector3f getRotation()
	{
		return rotation;
	}

	public float getPitch()
	{
		return rotation.x;
	}

	public float getYaw()
	{
		return rotation.y;
	}

	public float getRoll()
	{
		return rotation.z;
	}

	public void setRotationMinima(Vector3f rotationMinima)
	{
		this.rotationMinima = rotationMinima;
	}

	public Vector3f getRotationMinima()
	{
		return rotationMinima;
	}

	public void setRotationMaxima(Vector3f rotationMaxima)
	{
		this.rotationMaxima = rotationMaxima;
	}

	public Vector3f getRotationMaxima()
	{
		return rotationMaxima;
	}

	public boolean isRotationRestricted()
	{
		return restrictRotation;
	}

	public void restrictRotation(boolean restrictRotation)
	{
		this.restrictRotation = restrictRotation;
	}
	
	public void setScale(Vector3f scale)
	{
		this.scale = scale;
	}
	
	public Vector3f getScale()
	{
		return scale;
	}

	@Override
	public String getID()
	{
		return getStaticID();
	}

	public static String getStaticID()
	{
		return "TransformComponent";
	}
}
