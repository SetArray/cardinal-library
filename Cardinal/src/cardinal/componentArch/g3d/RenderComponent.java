package cardinal.componentArch.g3d;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.GL_TEXTURE10;
import static org.lwjgl.opengl.GL13.GL_TEXTURE11;
import static org.lwjgl.opengl.GL13.GL_TEXTURE12;
import static org.lwjgl.opengl.GL13.GL_TEXTURE13;
import static org.lwjgl.opengl.GL13.GL_TEXTURE14;
import static org.lwjgl.opengl.GL13.GL_TEXTURE15;
import static org.lwjgl.opengl.GL13.GL_TEXTURE16;
import static org.lwjgl.opengl.GL13.GL_TEXTURE17;
import static org.lwjgl.opengl.GL13.GL_TEXTURE18;
import static org.lwjgl.opengl.GL13.GL_TEXTURE19;
import static org.lwjgl.opengl.GL13.GL_TEXTURE2;
import static org.lwjgl.opengl.GL13.GL_TEXTURE20;
import static org.lwjgl.opengl.GL13.GL_TEXTURE21;
import static org.lwjgl.opengl.GL13.GL_TEXTURE22;
import static org.lwjgl.opengl.GL13.GL_TEXTURE23;
import static org.lwjgl.opengl.GL13.GL_TEXTURE24;
import static org.lwjgl.opengl.GL13.GL_TEXTURE25;
import static org.lwjgl.opengl.GL13.GL_TEXTURE26;
import static org.lwjgl.opengl.GL13.GL_TEXTURE27;
import static org.lwjgl.opengl.GL13.GL_TEXTURE28;
import static org.lwjgl.opengl.GL13.GL_TEXTURE29;
import static org.lwjgl.opengl.GL13.GL_TEXTURE3;
import static org.lwjgl.opengl.GL13.GL_TEXTURE30;
import static org.lwjgl.opengl.GL13.GL_TEXTURE31;
import static org.lwjgl.opengl.GL13.GL_TEXTURE4;
import static org.lwjgl.opengl.GL13.GL_TEXTURE5;
import static org.lwjgl.opengl.GL13.GL_TEXTURE6;
import static org.lwjgl.opengl.GL13.GL_TEXTURE7;
import static org.lwjgl.opengl.GL13.GL_TEXTURE8;
import static org.lwjgl.opengl.GL13.GL_TEXTURE9;
import static org.lwjgl.opengl.GL13.glActiveTexture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Component;
import cardinal.componentArch.Event;
import cardinal.componentArch.events.RenderEvent;
import cardinal.graphics.g3d.ModelTexture;
import cardinal.graphics.model.g3d.RawModel;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RenderComponent extends Component
{
	public static final int[] GL_ACTIVE_TEXTURES = new int[]
	{
			GL_TEXTURE0,
			GL_TEXTURE1,
			GL_TEXTURE2,
			GL_TEXTURE3,
			GL_TEXTURE4,
			GL_TEXTURE5,
			GL_TEXTURE6,
			GL_TEXTURE7,
			GL_TEXTURE8,
			GL_TEXTURE9,
			GL_TEXTURE10,
			GL_TEXTURE11,
			GL_TEXTURE12,
			GL_TEXTURE13,
			GL_TEXTURE14,
			GL_TEXTURE15,
			GL_TEXTURE16,
			GL_TEXTURE17,
			GL_TEXTURE18,
			GL_TEXTURE19,
			GL_TEXTURE20,
			GL_TEXTURE21,
			GL_TEXTURE22,
			GL_TEXTURE23,
			GL_TEXTURE24,
			GL_TEXTURE25,
			GL_TEXTURE26,
			GL_TEXTURE27,
			GL_TEXTURE28,
			GL_TEXTURE29,
			GL_TEXTURE30,
			GL_TEXTURE31 
		};
	
	private RawModel renderModel;
	private ModelTexture[] modelTextures;
	private RenderPropertiesComponent properties;
	
	public RenderComponent(RawModel renderModel, ModelTexture[] textures, RenderPropertiesComponent properties)
	{
		this.renderModel = renderModel;
		this.modelTextures = textures;
		this.properties = properties;
	}

	@Override
	public void update() {}
	
	private void render()
	{
		for(int i = modelTextures.length - 1; i >= 0; i--)
		{
			glActiveTexture(GL_ACTIVE_TEXTURES[i]);
			glBindTexture(GL_TEXTURE_2D, modelTextures[i].getTexture());

			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);

			if(properties.clampTextureOnEdges() && i == modelTextures.length - 1)
			{
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			}
			else
			{
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			}
		}

		GL30.glBindVertexArray(renderModel.getVAOID());
		{
			for(int i = 0; i < renderModel.getVertexAttributes(); i++)
			{
				GL20.glEnableVertexAttribArray(i);
			}

			if(properties.is3D())
				GL11.glDrawElements(GL11.GL_TRIANGLES, renderModel.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
			else
				GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, renderModel.getVertexCount());

			for(int i = 0; i < renderModel.getVertexAttributes(); i++)
			{
				GL20.glDisableVertexAttribArray(i);
			}
		}
		GL30.glBindVertexArray(0);

		for(int i = modelTextures.length - 1; i >= 0; i--)
		{
			glActiveTexture(GL_ACTIVE_TEXTURES[i]);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}


	@Override
	public void lateUpdate() {}


	@Override
	public boolean fireEvent(Event event)
	{
		if(event instanceof RenderEvent)
		{
			render();
			return true;
		}
		
		return false;
	}

	@Override
	public void awake() {}


	@Override
	public void start() {}


	@Override
	public void destroy() {}

	@Override
	public String getID()
	{
		return getStaticID();
	}

	public static String getStaticID()
	{
		return "RenderComponent";
	}	
}
