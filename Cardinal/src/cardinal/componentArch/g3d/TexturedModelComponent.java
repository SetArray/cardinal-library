package cardinal.componentArch.g3d;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Component;
import cardinal.componentArch.Event;
import cardinal.graphics.model.g3d.TexturedModel;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TexturedModelComponent extends Component
{
	protected TexturedModel renderModel;
	protected int textureIndex;

	public TexturedModelComponent(TexturedModel renderModel)
	{
		this.renderModel = renderModel;
	}

	@Override
	public void awake() {}

	@Override
	public void start() {}


	@Override
	public void update() {}
	
	@Override
	public boolean fireEvent(Event event)
	{
		return false;
	}

	@Override
	public void lateUpdate() {}


	@Override
	public void destroy() {}


	public TexturedModel getTexturedModel()
	{
		return renderModel;
	}
	
	public void setTextureIndex(int index)
	{
		this.textureIndex = index;
	}

	public int getTextureIndex()
	{
		return textureIndex;
	}

	public String getID()
	{
		return getStaticID();
	}

	public static String getStaticID()
	{
		return "TexturedModelComponent";
	}
}