package cardinal.componentArch;

import java.util.HashMap;
import java.util.Map;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public abstract class Event
{
	protected String id = "Event";
	protected Map<String, Object> paramaters;
	
	protected Event(String id)
	{
		this.id = id;
		paramaters = new HashMap<String, Object>();
	}
	
	protected void addParamater(String paramID, Object paramater)
	{
		paramaters.put(paramID, paramater);
	}
	
	public String getID()
	{
		return id;
	}
	
	public Object getParamater(String paramID)
	{
		return paramaters.get(paramID);
	}
}
