package cardinal.componentArch.g2d;

import java.awt.Font;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Event;
import cardinal.componentArch.g3d.TexturedModelComponent;
import cardinal.graphics.model.g2d.TextModel;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TextComponent extends TexturedModelComponent
{
	private TextModel textModel;
	
	public TextComponent(TextModel textModel)
	{
		super(textModel);
		this.textModel = textModel;
	}

	@Override
	public void update() {}
	

	@Override
	public void lateUpdate() {}
	

	@Override
	public void awake() {}
	

	@Override
	public void start() {}
	

	@Override
	public void destroy() {}
	

	public void prepare()
	{
	}

	public TextModel getTextModel()
	{
		return textModel;
	}

	public String getID()
	{
		return getStaticID();
	}
	
	public static String getStaticID()
	{
		return "TextComponent";
	}

	@Override
	public boolean fireEvent(Event event)
	{
		return false;
	}
}
