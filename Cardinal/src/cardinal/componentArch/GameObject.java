package cardinal.componentArch;

import java.util.ArrayList;
import java.util.List;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class GameObject
{
	private List<Component> components = new ArrayList<Component>();
	
	public GameObject(Blueprint blueprint)
	{
		for(Component component : blueprint.getComponents())
			addComponent(component);
	}
	
	public void update()
	{
		for(Component component : components)
			component.update();
	}
	
	public void lateUpdate()
	{
		for(Component component : components)
			component.lateUpdate();
	}
	
	public void addComponent(Component component)
	{
		components.add(component);
	}

	public Component getComponent(int index)
	{
		return components.get(index);
	}

	public Component getComponent(String componentID)
	{
		Component toReturn = null;
		
		for(Component component : components)
			if(component.getID().equals(componentID)) 
				toReturn =  component;
		
		return toReturn;
	}
	
	public boolean fireEvent(Event event)
	{
		boolean returnBool = false;
		
		for(Component component : components)
			returnBool = component.fireEvent(event) ? true : returnBool;
		
		return returnBool;
	}
}
