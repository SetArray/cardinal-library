package cardinal.console;

import cardinal.annotation.Preamble;


@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public interface ICommand
{
	/**
	 * Returns the command name used by the Console
	 * @return
	 */
	public String getCommandName();
	
	/**
	 * Returns a description of what the command is used for
	 * @return
	 */
	public String getCommandUsage();
	
	/**
	 * Executes the command
	 * @param args
	 */
	public void processCommand(String[] args);
}
