package cardinal.console;

import java.util.HashMap;
import java.util.Map;

import cardinal.annotation.Preamble;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public abstract class Console implements IConsole
{
	private boolean isOpen;
	private Map<String, Command> commands = new HashMap<String, Command>();

	public Console()
	{
		this.isOpen = false;
	}

	@Override
	public void registerCommand(Command command)
	{
		String commandName = command.getCommandName().toLowerCase();

		if(getCommand(commandName) == null)
			commands.put(commandName, command);
	}

	@Override
	public void processCommandLine(String commandLine)
	{
		String commandName = getCommandName(commandLine);
		String[] args = parseCommandLine(commandLine);
		
		Command command = null;
		
		try
		{
			command = getCommand(commandName);
			if(command == null)
				throw new CommandNotFoundException(commandName);
			
			command.processCommand(args);
		}
		catch(CommandNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void openConsole()
	{
		isOpen = true;
	}

	@Override
	public void closeConsole()
	{
		isOpen = false;
	}

	@Override
	public boolean isOpen()
	{
		return isOpen;
	}

	@Override
	public Map<String, Command> getCommands()
	{
		return commands;
	}

	@Override
	public Command getCommand(String name)
	{
		return commands.get(name.toLowerCase());
	}

	@Override
	public String[] parseCommandLine(String commandLine)
	{
		String[] args = commandLine.split("/")[1].split(" ");
		String[] passArgs = new String[args.length - 1];
		
		for(int i = 0; i < passArgs.length; i++)
			passArgs[i] = args[i + 1];
		
		return passArgs;
	}
	
	private String getCommandName(String commandLine)
	{
		return commandLine.split("/")[1].split(" ")[0];
	}
}
