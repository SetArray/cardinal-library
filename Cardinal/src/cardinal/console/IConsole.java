package cardinal.console;

import java.util.Map;

import cardinal.annotation.Preamble;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public interface IConsole
{
	public void registerCommand(Command command);
	
	/**
	 * Parses command entered into console into an array of the command and its arguments
	 * @param commandLine
	 * @return
	 */
	public void processCommandLine(String commandLine);
	
	public void update();
	public void render();
	public void openConsole();
	public void closeConsole();
	public boolean isOpen();
	public Map<String, Command> getCommands();
	public Command getCommand(String name);
	public String[] parseCommandLine(String commandLine);
}
