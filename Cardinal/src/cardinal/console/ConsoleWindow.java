package cardinal.console;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class ConsoleWindow
{
	private Console console;
	
	public ConsoleWindow(Console console)
	{
		this.console = console;
	}

	public void update()
	{

	}
	
	public void bindConsole(Console console)
	{
		this.console = console;
	}
	
	public Console getConsole()
	{
		return console;
	}
}
