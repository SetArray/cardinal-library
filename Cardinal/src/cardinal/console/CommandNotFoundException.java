package cardinal.console;

import cardinal.annotation.Preamble;

@SuppressWarnings("serial")
@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class CommandNotFoundException extends Exception
{	
	public CommandNotFoundException(final String command)
	{
		super("Command " + command + " not found");
	}
}
