package cardinal.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;
import cardinal.componentArch.g3d.TexturedModelComponent;
import cardinal.componentArch.g3d.TransformComponent;

/**
 * Shader to render static models
 */
@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class StaticShader extends ShaderProgram
{
	private static final String SRC_VERT = "shaders/static.vert";
	private static final String SRC_FRAG = "shaders/static.frag";
	private static final StaticShader instance = new StaticShader();
	
	public static StaticShader getInstance()
	{
		return instance;
	}
	
	public StaticShader()
	{
		super(SRC_VERT, SRC_FRAG);
	}
	
	@Override
	public void addUniforms()
	{
		addUniform("projectionMatrix");
		addUniform("viewMatrix");
		addUniform("modelMatrix");
		
		addUniform("numberOfRows");
		addUniform("indexOffset");
		addUniform("opacity");
		
		addUniform("clipPlane");
	}
	
	@Override
	public void bindAttributes()
	{
		bindAttribute(0, "vertices");
		bindAttribute(1, "texCoords");
		bindAttribute(2, "normals");
	}
	
	@Override
	public void loadShaderData(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix, GameObject gameObject, Object... externalData)
	{
		TexturedModelComponent texComponent = ((TexturedModelComponent)gameObject.getComponent(1));

		setUniform("projectionMatrix", projectionMatrix);
		setUniform("viewMatrix", viewMatrix);
		setUniform("modelMatrix", modelMatrix);
		
		setUniform("numberOfRows", texComponent.getTexturedModel().getModelTexture(texComponent.getTextureIndex()).getNumberOfRows());
		setUniform("indexOffset", texComponent.getTexturedModel().getModelTexture(texComponent.getTextureIndex()).getAtlasIndexOffset(0));
		setUniform("opacity", 1.0f);

		Vector4f clipPlane = (externalData.length > 0) ? (Vector4f) externalData[0] : new Vector4f(0, 0, 0, 0);
			setUniform("clipPlane", clipPlane);
	}
}
