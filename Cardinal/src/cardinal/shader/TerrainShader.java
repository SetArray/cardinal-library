package cardinal.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TerrainShader extends ShaderProgram
{
	private static final String SRC_VERT = "shaders/heightmap.vert";
	private static final String SRC_FRAG = "shaders/heightmap.frag";
	
	private static final TerrainShader instance = new TerrainShader();
	
	public static TerrainShader getInstance()
	{
		return instance;
	}
	
	public TerrainShader()
	{
		super(SRC_VERT, SRC_FRAG);
	}

	@Override
	public void addUniforms()
	{
		addUniform("projectionMatrix");
		addUniform("viewMatrix");
		addUniform("modelMatrix");
		
		addUniform("backgroundTexture");
		addUniform("rTexture");
		addUniform("gTexture");
		addUniform("bTexture");
		addUniform("blendMapTexture");

		addUniform("clipPlane");
	}

	@Override
	public void bindAttributes()
	{
		super.bindAttribute(0, "vertices");
		super.bindAttribute(1, "texCoords");
		super.bindAttribute(2, "normals");
	}
	
	@Override
	public void loadShaderData(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix, GameObject gameObject, Object... externalData)
	{
		setUniform("projectionMatrix", projectionMatrix);
		setUniform("viewMatrix", viewMatrix);
		setUniform("modelMatrix", modelMatrix);
		
		setUniform("backgroundTexture", 0);
		setUniform("rTexture", 1);
		setUniform("gTexture", 2);
		setUniform("bTexture", 3);
		setUniform("blendMapTexture", 4);

		Vector4f clipPlane = (externalData.length > 0) ? (Vector4f) externalData[0] : new Vector4f(0, 0, 0, 0);
		setUniform("clipPlane", clipPlane);
	}
}