package cardinal.shader;

import org.lwjgl.util.vector.Matrix4f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public interface IShader
{
	public void addUniforms();
	public void loadShaderData(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix, GameObject gameObject, Object... externalData);
	public void bindAttributes();
}
