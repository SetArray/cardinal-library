package cardinal.shader;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_INFO_LOG_LENGTH;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4;
import static org.lwjgl.opengl.GL20.glValidateProgram;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;
import cardinal.util.FileUtil;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public abstract class ShaderProgram implements IShader
{
	private int programID;
	private int vertexShaderID, fragmentShaderID;
	private String srcVertexShader, srcFragmentShader;
	
	private Map<String, Integer> uniforms = new HashMap<String, Integer>();
	
	public ShaderProgram(String vertShaderSrc, String fragShaderSrc)
	{
		programID = glCreateProgram();
		addVertexShader(vertShaderSrc);
		addFragmentShader(fragShaderSrc);
		compileShader();
		bindAttributes();
		addUniforms();
	}
	
	protected void addVertexShader(String src)
	{
		srcVertexShader = src;

		vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		if (vertexShaderID == 0)
			throw new RuntimeException("Could not create vertex shader: " + srcVertexShader);

		String shaderCode = null;
		try
		{
			shaderCode = FileUtil.loadResourceAsString(src);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		glShaderSource(vertexShaderID, shaderCode);
	}

	protected void addFragmentShader(String src)
	{
		srcFragmentShader = src;

		fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		if (fragmentShaderID == 0)
			throw new RuntimeException("Could not create fragment shader: " + srcFragmentShader);

		String shaderCode = null;
		try
		{
			shaderCode = FileUtil.loadResourceAsString(src);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		glShaderSource(fragmentShaderID, shaderCode);
	}

	protected void compileShader()
	{
		glCompileShader(vertexShaderID);
		checkCompileStatus(vertexShaderID);
		glCompileShader(fragmentShaderID);
		checkCompileStatus(fragmentShaderID);
		
		glAttachShader(programID, vertexShaderID);
		glAttachShader(programID, fragmentShaderID);

		glLinkProgram(programID);
		if (glGetProgrami(programID, GL_LINK_STATUS) == GL_FALSE)
			throw new RuntimeException("Could not link shader: " + glGetProgramInfoLog(programID, 1000));

		glValidateProgram(programID);
		if (glGetProgrami(programID, GL_VALIDATE_STATUS) == GL_FALSE)
			throw new RuntimeException("Could not validate shader: " + glGetProgramInfoLog(programID, 1000));
	}
	
	public void start()
	{
		GL20.glUseProgram(programID);
	}
	
	public void stop()
	{
		GL20.glUseProgram(0);
	}
	
	public void cleanUp()
	{
		stop();
		GL20.glDetachShader(programID, vertexShaderID);
		GL20.glDetachShader(programID, fragmentShaderID);
		GL20.glDeleteShader(vertexShaderID);
		GL20.glDeleteShader(fragmentShaderID);
		GL20.glDeleteProgram(programID);
	}

	private void checkCompileStatus(int shaderHandle)
	{
		int shaderStatus = glGetShaderi(shaderHandle, GL_COMPILE_STATUS);
		
		if (shaderStatus == GL_FALSE)
		{
			String shaderSrc;
			if(shaderHandle == vertexShaderID) shaderSrc = srcVertexShader;
			else shaderSrc = srcFragmentShader;
			throw new IllegalStateException("Compilation error for shader [" + shaderSrc + "]\n"
					+ glGetShaderInfoLog(shaderHandle, glGetShaderi(shaderHandle, GL_INFO_LOG_LENGTH)));
		}
	}
	
	protected void bindAttribute(int attribute, String name)
	{
		GL20.glBindAttribLocation(programID, attribute, name);
	}
	
	protected void addUniform(String uniform)
	{
	    int loc = glGetUniformLocation(programID, uniform);
	    uniforms.put(uniform, loc);
	}
	
	protected int getUniformLocation(String uniform)
	{
		int location = uniforms.get(uniform);
		return (uniform != null) ? location : null;
	}
	
	protected void setUniform(String uniform, Object arg)
	{
		int location = getUniformLocation(uniform);
		
		if(arg instanceof Integer)
			glUniform1i(location, (int) arg);
		if(arg instanceof Float)
			glUniform1f(location, (float) arg);
		if(arg instanceof Vector2f)
		{
			Vector2f vec2 = (Vector2f) arg;
			glUniform2f(location, vec2.x, vec2.y);
		}
		if(arg instanceof Vector3f)
		{
			Vector3f vec3 = (Vector3f) arg;
			glUniform3f(location, vec3.x, vec3.y, vec3.z);
		}
		if(arg instanceof Vector4f)
		{
			Vector4f vec4 = (Vector4f) arg;
			glUniform4f(location, vec4.x, vec4.y, vec4.z, vec4.w);
		}
		if(arg instanceof Matrix4f)
		{
			Matrix4f mat4 = (Matrix4f) arg;
			FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
	        buffer.put(mat4.m00);
	        buffer.put(mat4.m01);
	        buffer.put(mat4.m02);
	        buffer.put(mat4.m03);
	        buffer.put(mat4.m10);
	        buffer.put(mat4.m11);
	        buffer.put(mat4.m12);
	        buffer.put(mat4.m13);
	        buffer.put(mat4.m20);
	        buffer.put(mat4.m21);
	        buffer.put(mat4.m22);
	        buffer.put(mat4.m23);
	        buffer.put(mat4.m30);
	        buffer.put(mat4.m31);
	        buffer.put(mat4.m32);
	        buffer.put(mat4.m33);
	        buffer.flip();
			glUniformMatrix4(location, false, buffer);
		}
	}
	
	public int getProgramID()
	{
		return programID;
	}
}