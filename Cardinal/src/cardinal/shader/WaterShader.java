package cardinal.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;
import cardinal.componentArch.g3d.TransformComponent;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class WaterShader extends ShaderProgram
{
	private static final String SRC_VERT = "shaders/water.vert";
	private static final String SRC_FRAG = "shaders/water.frag";
	private static final WaterShader instance = new WaterShader();
	
	public static WaterShader getInstance()
	{
		return instance;
	}
	
	private WaterShader()
	{
		super(SRC_VERT, SRC_FRAG);
	}
	
	@Override
	public void addUniforms()
	{
		addUniform("projectionMatrix");
		addUniform("viewMatrix");
		addUniform("modelMatrix");
		
		addUniform("cameraPosition");

		addUniform("reflectionTexture");
		addUniform("refractionTexture");
		addUniform("refractionDepthTexture");
		addUniform("rippleDUDVTexture");
		
		addUniform("moveFactor");
	}
	
	@Override
	public void bindAttributes()
	{
		bindAttribute(0, "vertices");
		bindAttribute(1, "texCoords");
		bindAttribute(2, "normals");
	}
	
	private float moveFactor = 0.0f;

	@Override
	public void loadShaderData(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix, GameObject gameObject, Object... externalData)
	{
		setUniform("projectionMatrix", projectionMatrix);
		setUniform("viewMatrix", viewMatrix);
		setUniform("modelMatrix", modelMatrix);

		setUniform("cameraPosition", (Vector3f) externalData[0]);
		
		setUniform("reflectionTexture", 0);
		setUniform("refractionTexture", 1);
		setUniform("refractionDepthTexture", 2);
		setUniform("rippleDUDVTexture", 3);
		
		moveFactor += 0.0004f;
		moveFactor %= 1.0f;
		setUniform("moveFactor", moveFactor);
	}
}
