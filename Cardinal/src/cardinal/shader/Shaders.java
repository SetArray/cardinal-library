package cardinal.shader;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Shaders
{
	//2D Shaders
	public static final TextShader TEXT_SHADER = TextShader.getInstance();
	
	//3D Shaders
	public static final StaticShader STATIC_SHADER = StaticShader.getInstance();
	public static final TerrainShader TERRAIN_SHADER = TerrainShader.getInstance();
	public static final WaterShader WATER_SHADER = WaterShader.getInstance();
}
