package cardinal.shader;

import org.lwjgl.util.vector.Matrix4f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;
import cardinal.componentArch.g2d.TextComponent;
import cardinal.componentArch.g3d.TransformComponent;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TextShader extends ShaderProgram
{
	private static final String SRC_VERT = "shaders/text.vert";
	private static final String SRC_FRAG = "shaders/text.frag";
	private static final TextShader instance = new TextShader();
	
	public static TextShader getInstance()
	{
		return instance;
	}
	
	public TextShader()
	{
		super(SRC_VERT, SRC_FRAG);
	}
	
	@Override
	public void addUniforms()
	{
		addUniform("orthogonalProjectionMatrix");
		addUniform("viewMatrix");
		addUniform("modelMatrix");
		
		addUniform("position");
		addUniform("fontColor");
	}
	
	@Override
	public void bindAttributes()
	{
		bindAttribute(0, "vertices");
		bindAttribute(1, "texCoords");
	}
	
	@Override
	public void loadShaderData(Matrix4f orthogonalMatrix, Matrix4f viewMatrix, Matrix4f modelMatrix, GameObject gameObject, Object... externalData)
	{
		setUniform("orthogonalProjectionMatrix", orthogonalMatrix);
		setUniform("viewMatrix", viewMatrix);
		setUniform("modelMatrix", modelMatrix);
		
		setUniform("position", ((TransformComponent) gameObject.getComponent(TransformComponent.getStaticID())).getPosition2f());
		setUniform("fontColor", ((TextComponent) gameObject.getComponent(TextComponent.getStaticID())).getTextModel().getColor());//gameObject.getTextComponent().getTextModel().getFontText().getColor());
	}
}