package cardinal.controls;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Controller
{
	private static Controller controller = new Controller();

	public static final int KEY_NONE = Keyboard.KEY_NONE;
	public static final int KEY_ESCAPE = Keyboard.KEY_ESCAPE;
	public static final int KEY_1 = Keyboard.KEY_1;
	public static final int KEY_2 = Keyboard.KEY_2;
	public static final int KEY_3 = Keyboard.KEY_3;
	public static final int KEY_4 = Keyboard.KEY_4;
	public static final int KEY_5 = Keyboard.KEY_5;
	public static final int KEY_6 = Keyboard.KEY_6;
	public static final int KEY_7 = Keyboard.KEY_7;
	public static final int KEY_8 = Keyboard.KEY_8;
	public static final int KEY_9 = Keyboard.KEY_9;
	public static final int KEY_0 = Keyboard.KEY_0;
	public static final int KEY_DASH = Keyboard.KEY_SUBTRACT;
	public static final int KEY_EQUALS = Keyboard.KEY_EQUALS;
	public static final int KEY_BACK = Keyboard.KEY_BACK;
	public static final int KEY_TAB = Keyboard.KEY_TAB;
	public static final int KEY_Q = Keyboard.KEY_Q;
	public static final int KEY_W = Keyboard.KEY_W;
	public static final int KEY_E = Keyboard.KEY_E;
	public static final int KEY_R = Keyboard.KEY_R;
	public static final int KEY_T = Keyboard.KEY_T;
	public static final int KEY_Y = Keyboard.KEY_Y;
	public static final int KEY_U = Keyboard.KEY_U;
	public static final int KEY_I = Keyboard.KEY_I;
	public static final int KEY_O = Keyboard.KEY_O;
	public static final int KEY_P = Keyboard.KEY_P;
	public static final int KEY_LBRACKET = Keyboard.KEY_LBRACKET;
	public static final int KEY_RBRACKET = Keyboard.KEY_RBRACKET;
	public static final int KEY_RETURN = Keyboard.KEY_RETURN;
	public static final int KEY_LCONTROL = Keyboard.KEY_LCONTROL;
	public static final int KEY_A = Keyboard.KEY_A;
	public static final int KEY_S = Keyboard.KEY_S;
	public static final int KEY_D = Keyboard.KEY_D;
	public static final int KEY_F = Keyboard.KEY_F;
	public static final int KEY_G = Keyboard.KEY_G;
	public static final int KEY_H = Keyboard.KEY_H;
	public static final int KEY_J = Keyboard.KEY_J;
	public static final int KEY_K = Keyboard.KEY_K;
	public static final int KEY_L = Keyboard.KEY_L;
	public static final int KEY_SEMICOLON = Keyboard.KEY_SEMICOLON;
	public static final int KEY_APOSTROPHE = Keyboard.KEY_APOSTROPHE;
	public static final int KEY_GRAVE = Keyboard.KEY_GRAVE;
	public static final int KEY_LSHIFT = Keyboard.KEY_LSHIFT;
	public static final int KEY_BACKSLASH = Keyboard.KEY_BACKSLASH;
	public static final int KEY_COMMA = Keyboard.KEY_COMMA;
	public static final int KEY_PERIOD = Keyboard.KEY_PERIOD;
	public static final int KEY_SLASH = Keyboard.KEY_SLASH;
	public static final int KEY_RSHIFT = Keyboard.KEY_RSHIFT;
	public static final int KEY_ASTERISK = Keyboard.KEY_MULTIPLY;
	public static final int KEY_LMENU = Keyboard.KEY_LMENU;
	public static final int KEY_SPACE = Keyboard.KEY_SPACE;
	public static final int KEY_CAPSLOCK = Keyboard.KEY_CAPITAL;
	public static final int KEY_F1 = Keyboard.KEY_F1;
	public static final int KEY_F2 = Keyboard.KEY_F2;
	public static final int KEY_F3 = Keyboard.KEY_F3;
	public static final int KEY_F4 = Keyboard.KEY_F4;
	public static final int KEY_F5 = Keyboard.KEY_F5;
	public static final int KEY_F6 = Keyboard.KEY_F6;
	public static final int KEY_F7 = Keyboard.KEY_F7;
	public static final int KEY_F8 = Keyboard.KEY_F8;
	public static final int KEY_F9 = Keyboard.KEY_F9;
	public static final int KEY_F10 = Keyboard.KEY_F10;
	public static final int KEY_NUMLOCK = Keyboard.KEY_NUMLOCK;
	public static final int KEY_SCROLLLOCK = Keyboard.KEY_SCROLL;
	public static final int KEY_NUMPAD7 = Keyboard.KEY_NUMPAD7;
	public static final int KEY_NUMPAD8 = Keyboard.KEY_NUMPAD8;
	public static final int KEY_NUMPAD9 = Keyboard.KEY_NUMPAD9;
	public static final int KEY_SUBTRACT = Keyboard.KEY_SUBTRACT;
	public static final int KEY_NUMPAD4 = Keyboard.KEY_NUMPAD4;
	public static final int KEY_NUMPAD5 = Keyboard.KEY_NUMPAD5;
	public static final int KEY_NUMPAD6 = Keyboard.KEY_NUMPAD6;
	public static final int KEY_ADD = Keyboard.KEY_ADD;
	public static final int KEY_NUMPAD1 = Keyboard.KEY_NUMPAD1;
	public static final int KEY_NUMPAD2 = Keyboard.KEY_NUMPAD2;
	public static final int KEY_NUMPAD3 = Keyboard.KEY_NUMPAD3;
	public static final int KEY_NUMPAD0 = Keyboard.KEY_NUMPAD0;
	public static final int KEY_DECIMAL = Keyboard.KEY_DECIMAL;
	public static final int KEY_F11 = Keyboard.KEY_F11;
	public static final int KEY_F12 = Keyboard.KEY_F12;
	public static final int KEY_F13 = Keyboard.KEY_F13;
	public static final int KEY_F14 = Keyboard.KEY_F14;
	public static final int KEY_F15 = Keyboard.KEY_F15;
	public static final int KEY_F16 = Keyboard.KEY_F16;
	public static final int KEY_F17 = Keyboard.KEY_F17;
	public static final int KEY_F18 = Keyboard.KEY_F18;
	public static final int KEY_KANA = Keyboard.KEY_KANA;
	public static final int KEY_F19 = Keyboard.KEY_F19;
	public static final int KEY_CONVERT = Keyboard.KEY_CONVERT;
	public static final int KEY_NOCONVERT = Keyboard.KEY_NOCONVERT;
	public static final int KEY_YEN = Keyboard.KEY_YEN;
	public static final int KEY_CIRCUMFLEX = Keyboard.KEY_CIRCUMFLEX;
	public static final int KEY_AT = Keyboard.KEY_AT;
	public static final int KEY_COLON = Keyboard.KEY_COLON;
	public static final int KEY_UNDERLINE = Keyboard.KEY_UNDERLINE;
	public static final int KEY_KANJI = Keyboard.KEY_KANJI;
	public static final int KEY_STOP = Keyboard.KEY_STOP;
	public static final int KEY_AX = Keyboard.KEY_AX;
	public static final int KEY_NUMPADENTER = Keyboard.KEY_NUMPADENTER;
	public static final int KEY_RCONTROL = Keyboard.KEY_RCONTROL;
	public static final int KEY_SECTION = Keyboard.KEY_SECTION;
	public static final int KEY_NUMPADCOMMA = Keyboard.KEY_NUMPADCOMMA;
	public static final int KEY_NUMPADSLASH = Keyboard.KEY_DIVIDE;
	public static final int KEY_SYSRQ = Keyboard.KEY_SYSRQ;
	public static final int KEY_RALT = Keyboard.KEY_RMENU;
	public static final int KEY_FUNCTION = Keyboard.KEY_FUNCTION;
	public static final int KEY_PAUSE = Keyboard.KEY_PAUSE;
	public static final int KEY_HOME = Keyboard.KEY_HOME;
	public static final int KEY_UP = Keyboard.KEY_UP;
	public static final int KEY_PRIOR = Keyboard.KEY_PRIOR;
	public static final int KEY_LEFT = Keyboard.KEY_LEFT;
	public static final int KEY_RIGHT = Keyboard.KEY_RIGHT;
	public static final int KEY_END = Keyboard.KEY_END;
	public static final int KEY_DOWN = Keyboard.KEY_DOWN;
	public static final int KEY_NEXT = Keyboard.KEY_END;
	public static final int KEY_INSERT = Keyboard.KEY_INSERT;
	public static final int KEY_DELETE = Keyboard.KEY_DELETE;
	public static final int KEY_LMETA = Keyboard.KEY_LMETA;
	public static final int KEY_RMETA = Keyboard.KEY_RMETA;
	public static final int KEY_APPS = Keyboard.KEY_APPS;
	public static final int KEY_POWER = Keyboard.KEY_POWER;
	public static final int KEY_SLEEP = Keyboard.KEY_SLEEP;
	public static final int MOUSE_LEFTCLICK = 0;
	public static final int MOUSE_RIGHTCLICK = 1;
	public static final int MOUSE_MIDDLECLICK = 2;
	
	private static ArrayList<EventState> keyboardEvents, mouseEvents;
	private static int mouseDX, mouseDY;
	
	private enum EventState
	{
		NONE, PRESSED, DOWN, RELEASED;
	}

	public Controller()
	{
		mouseDX = 0; 
		mouseDY = 0;
		mouseEvents = new ArrayList<EventState>();
		for (int i = 0; i < Mouse.getButtonCount(); i++)
		{
			mouseEvents.add(EventState.NONE);
		}

		keyboardEvents = new ArrayList<EventState>();
		for (int i = 0; i < Keyboard.KEYBOARD_SIZE; i++)
		{
			keyboardEvents.add(EventState.NONE);
		}
	}

	private void Update()
	{
		resetKeys();
		for (int i = 0; i < Keyboard.KEYBOARD_SIZE; i++)
		{
			if (Keyboard.isKeyDown(i))
				keyboardEvents.set(i, EventState.DOWN);
		}
		while (Keyboard.next())
		{
			int key = Keyboard.getEventKey();
			if (key < 0)
				continue;

			if (Keyboard.getEventKeyState())
			{
				if (!Keyboard.isRepeatEvent())
				{
					keyboardEvents.set(key, EventState.PRESSED);
				}
			}
			else
			{
				keyboardEvents.set(key, EventState.RELEASED);
			}
		}

		resetMouse();
		for (int i = 0; i < Mouse.getButtonCount(); i++)
		{
			if (Mouse.isButtonDown(i))
				mouseEvents.set(i, EventState.DOWN);
		}
		while (Mouse.next())
		{
			int button = Mouse.getEventButton();
			if (button < 0)
				continue;
			if (Mouse.getEventButtonState())
			{
				mouseEvents.set(button, EventState.PRESSED);
			}
			else
			{
				mouseEvents.set(button, EventState.RELEASED);
			}
		}

		mouseDX = Mouse.getDX();
		mouseDY = Mouse.getDY();
	}

	private void resetKeys()
	{
		for (int i = 0; i < Keyboard.KEYBOARD_SIZE; i++)
		{
			keyboardEvents.set(i, EventState.NONE);
		}
	}

	private void resetMouse()
	{
		for (int i = 0; i < Mouse.getButtonCount(); i++)
		{
			mouseEvents.set(i, EventState.NONE);
		}
	}

	private boolean KeyDown(int key)
	{
		return keyboardEvents.get(key) == EventState.DOWN;
	}

	private boolean KeyPressed(int key)
	{
		return keyboardEvents.get(key) == EventState.PRESSED;
	}

	private boolean KeyReleased(int key)
	{
		return keyboardEvents.get(key) == EventState.RELEASED;
	}

	private boolean MouseButtonDown(int key)
	{
		return mouseEvents.get(key) == EventState.DOWN;
	}

	private boolean MouseButtonPressed(int key)
	{
		return mouseEvents.get(key) == EventState.PRESSED;
	}

	private boolean MouseButtonReleased(int key)
	{
		return mouseEvents.get(key) == EventState.RELEASED;
	}
	
	public static int getMouseDX()
	{
		return mouseDX;
	}
	
	public static int getMouseDY()
	{
		return mouseDY;
	}

	public static boolean isKeyDown(int key)
	{
		return controller.KeyDown(key);
	}

	public static boolean isKeyPressed(int key)
	{
		return controller.KeyPressed(key);
	}

	public static boolean isKeyReleased(int key)
	{
		return controller.KeyReleased(key);
	}

	public static boolean isMouseButtonDown(int key)
	{
		return controller.MouseButtonDown(key);
	}

	public static boolean isMouseButtonPressed(int key)
	{
		return controller.MouseButtonPressed(key);
	}

	public static boolean isMouseButtonReleased(int key)
	{
		return controller.MouseButtonReleased(key);
	}
	
	public static void grabMouse(boolean grab)
	{
		Mouse.setGrabbed(grab);
	}
	
	public static boolean isMouseGrabbed()
	{
		return Mouse.isGrabbed();
	}

	public static int getMouseX()
	{
		return Mouse.getX();
	}
	
	public static int getMouseY()
	{
		return Display.getHeight() - Mouse.getY() - 1;
	}
	
	public static void update()
	{
		controller.Update();
	}
}
