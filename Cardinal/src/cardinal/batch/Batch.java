package cardinal.batch;

import java.util.ArrayList;
import java.util.List;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Batch
{
	private String id;
	private List<Task> tasks;
	
	public Batch(String id)
	{
		tasks = new ArrayList<Task>();
		this.id = id;
	}

	protected void execute() throws Exception
	{
		for(Task task : tasks)
		{
			try
			{
				task.call();
			}
			catch(Exception e)
			{
				throw new Exception("Task(" + task.getID() + ") in Batch(" + id + ")", e);
			}
		}
	}
	
	public void addTask(Task task)
	{
		tasks.add(task);
	}
	
	public void removeTask(Task task)
	{
		tasks.remove(task);
	}
	
	public void destroy()
	{
		tasks.clear();
	}
	
	protected List<Task> getTasks()
	{
		return tasks;
	}
	
	public String getID()
	{
		return id;
	}
}
