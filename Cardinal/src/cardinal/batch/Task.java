package cardinal.batch;

import java.util.concurrent.Callable;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public abstract class Task implements Callable<Object>
{
	private String id;
	
	public Task(String id)
	{
		this.id = id;
	}
	
	public String getID()
	{
		return id;
	}
}
