package cardinal.batch;

import cardinal.annotation.Preamble;


@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Executor
{
	private String id;
	private Batch[] batches;
	
	public Executor(String id, Batch... batches)
	{
		this.id = id;
		this.batches = batches;
	}
	
	public void execute()
	{
		try
		{
			for(Batch batch : batches)
				batch.execute();
		}
		catch(Exception e)
		{

			System.err.println("Executor failed to execute " + e.getMessage() + "!");
			e.printStackTrace();
		}
	}
	
	public String debugTree()
	{
		String tree = "--Begin Tree--";
		
		
		tree += "\nExecutor(" + id + "):";
		for(Batch batch : batches)
		{
			tree += "\n\tBatch(" + batch.getID() + "):";
			
			for(Task task : batch.getTasks())
				tree += "\n\t\tTask:(" + task.getID() + ")";
		}
		
		tree += "\n--End Tree--";
		
		return tree;
	}
	
	public String getID()
	{
		return id;
	}
}