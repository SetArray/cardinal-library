package cardinal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Preamble
(
		dev="Tyler", 
		created="01-16-17", 
		last_modified="01-16-17"
)
@Description("Denotes mechanism of the Component Based Architecture")
public @interface Component
{

}
