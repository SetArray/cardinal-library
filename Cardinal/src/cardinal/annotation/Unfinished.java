package cardinal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates a piece of work as unfinished or incomplete
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.ANNOTATION_TYPE, ElementType.PACKAGE})
@Inherited
@Preamble
(
		dev="Tyler", 
		created="01-15-17", 
		last_modified="01-15-17"
)
public @interface Unfinished
{
	public enum Priority {LOW, MEDIUM, HIGH}
	
	String value();
	String[] developers() default "";
	Priority priority() default Priority.MEDIUM;
}
