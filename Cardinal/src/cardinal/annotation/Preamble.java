package cardinal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Preamble
(
		dev="Tyler", 
		created="01-15-17", 
		last_modified="01-15-17"
)
@Description("Annotation used to store basic class info")
public @interface Preamble
{
	String dev();
	String created();
	String last_modified();
}
