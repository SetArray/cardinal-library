package cardinal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @param field <{@link String}> - name of the field being annotated
 * @param name <{@link String}> - name designated to Font
 * @param size <{@link Integer}> - text point size
 * @param weight <{@link Float}> - stroke weight
 * @param posture <{@link Integer}> - algorithmic slant
 * @param condensation <{@link Integer}> - glyph seperation distance
 * @param justification <{@link Float}> - alignment of text in bounds
 * @param kerning <{@link Boolean}> - horizontal glyph advance
 * @param underline <{@link Boolean}> - line running beneath text
 * @param strikethrough <{@link Boolean}> - line running through text
 * @param overline <{@link Boolean}> - line running above text
 * @param superscript <{@link Boolean}> - small vertically raised text
 * @param subscript <{@link Boolean}> - small vertically dropped text
 * @param smallcaps <{@link Boolean}> - majuscule text of minuscule height/weight
 * @param foreground_color <{@link Integer}> - text foreground color
 * @param foreground_opacity <{@link Float}> - text foreground opacity
 * @param highlight_color <{@link Integer}> - text background color
 * @param highlight_opacity <{@link Float}> - text background opacity
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Preamble
(
		dev="Tyler", 
		created="01-15-17", 
		last_modified="01-15-17"
)
@Description("No idea yet")
public @interface CardinalFont
{
	Class<?> type();
	String field();
	int size();
	String name() default "<?>";
	float weight() default 0.0f;
	int posture() default 0;
	int condensation() default 0;
	float justification() default 0.0f; //-1.0f - 1.0f (left to right)
	boolean kerning() default false;
	boolean underline() default false;
	boolean strikethrough() default false;
	boolean overline() default false;
	boolean superscript() default false;
	boolean subscript() default false;
	boolean smallcaps() default false;
	int foreground_color() default -16777216; //black
	float foreground_opacity() default 1.0f;
	int highlight_color() default 0;
	float highlight_opacity() default 0.0f;
}
