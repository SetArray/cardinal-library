package cardinal.util;

import java.nio.FloatBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class FloatUtil
{
	public static float[][] cloneFloatArray(float[][] arr)
	{
		float[][] returnArray = new float[arr.length][];

		for(int i = 0; i < arr.length; i++)
			returnArray[i] = arr[i].clone();

		return returnArray;
	}

	public static FloatBuffer reserveFloatData(int size)
	{
		FloatBuffer data = BufferUtils.createFloatBuffer(size);
		return data;
	}

	public static float[] floatListToArray(List<Float> list)
	{
		float[] array = new float[list.size()];
		for(int i = 0; i < array.length; i++)
		{
			array[i] = list.get(i);
		}

		return array;
	}
}
