package cardinal.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.TextureLoader;

import cardinal.annotation.Preamble;
import cardinal.graphics.RawImage;
import cardinal.graphics.g3d.ModelTexture;
import cardinal.graphics.model.g3d.HeightmapModel;
import cardinal.graphics.model.g3d.ModelLoader3D;
import cardinal.graphics.model.g3d.RawModel;
import cardinal.text.bitmapfont.g2d.BitmapFontMetaData;
import cardinal.text.bitmapfont.g2d.BitmapGlyph;
import cardinal.text.bitmapfont.g2d.BitmapTypeFace;
import cardinal.text.g2d.Line;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class FileUtil
{
	public static InputStream loadResourceAsStream(String path)
	{
		return FileUtil.class.getClassLoader().getResourceAsStream(path);
	}

	public static String loadResourceAsString(String path) throws IOException
	{
		InputStream stream = loadResourceAsStream(path);

		InputStreamReader streamReader = new InputStreamReader(stream);
		BufferedReader reader = new BufferedReader(streamReader);

		String line;
		String fileText = "";

		while((line = reader.readLine()) != null)
		{
			fileText += line + "\n";
		}

		streamReader.close();
		reader.close();
		return fileText;
	}

	public static int loadResourceAsTexture(String path) throws IOException
	{
		String extension = path.substring(path.length() - 3); //File extension (png, jpg, etc)

		return TextureLoader.getTexture(extension, FileUtil.loadResourceAsStream(path)).getTextureID();
	}

	public static BitmapTypeFace loadTypeFace(String controlFilePath, String bitmapPath) throws IOException
	{
		BitmapFontMetaData controlData = null;
		int bitmap = loadResourceAsTexture(bitmapPath);

		HashSet<BitmapGlyph> characters = new HashSet<>();
		Map<String, String> lineData = new HashMap<String, String>();

		int[] padding = null;
		float paddingWidth = 0.0f;
		float paddingHeight = 0.0f;

		float yScreenSpaceConversion = 0.0f;
		float xScreenSpaceConversion = 0.0f;

		float imageSize = 0.0f;
		float spaceWidth = 0.0f;

		int id;
		float width;
		float height;

		float xTex;
		float yTex;
		float xTexSize;
		float yTexSize;

		float xOff;
		float yOff;
		float quadWidth;
		float quadHeight;
		float xAdvance;

		InputStream stream = FileUtil.loadResourceAsStream(controlFilePath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

		String line;
		String[] valuePairs;

		while((line = reader.readLine()) != null)
		{
			for(String part : line.split(" "))
			{
				valuePairs = part.split("=");
				if(valuePairs.length == 2)
				{
					lineData.put(valuePairs[0], valuePairs[1]);
				}
			}

			if(line.startsWith(BitmapFontMetaData.TAG_INFO))
			{
				String[] rawPaddingValues = lineData.get(BitmapFontMetaData.TAG_PADDING).split(",");
				padding = new int[rawPaddingValues.length];

				for(int i = 0; i < padding.length; i++)
				{
					padding[i] = Integer.parseInt(rawPaddingValues[i]);
				}

				paddingWidth = padding[BitmapFontMetaData.PADDING_LEFT_INDEX] + padding[BitmapFontMetaData.PADDING_RIGHT_INDEX];
				paddingHeight = padding[BitmapFontMetaData.PADDING_TOP_INDEX] + padding[BitmapFontMetaData.PADDING_BOTTOM_INDEX];
			}
			if(line.startsWith(BitmapFontMetaData.TAG_CHARS))
			{
				imageSize = Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_SCALEW));
				
				yScreenSpaceConversion = Line.LINE_HEIGHT / (float) Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_LINEHEIGHT));
				xScreenSpaceConversion = yScreenSpaceConversion / Window.getAspect();

			}
			if(line.startsWith(BitmapFontMetaData.TAG_CHAR) && !line.startsWith(BitmapFontMetaData.TAG_CHARS))
			{
				BitmapGlyph character = null;
				id = Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_ID));

				if(id == BitmapGlyph.SPACE_ASCII)
				{
					spaceWidth = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_XADVANCE)) - paddingWidth) * yScreenSpaceConversion;
					continue;
				}

				width = Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_WIDTH)) - paddingWidth;
				height = Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_HEIGHT)) - paddingHeight;

				xTex = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_X)) + padding[BitmapFontMetaData.PADDING_LEFT_INDEX]) / imageSize;
				yTex = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_Y)) + padding[BitmapFontMetaData.PADDING_TOP_INDEX]) / imageSize;
				xTexSize = width / imageSize;
				yTexSize = height / imageSize;

				xOff = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_XOFFSET)) + padding[BitmapFontMetaData.PADDING_LEFT_INDEX]) * xScreenSpaceConversion;
				yOff = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_YOFFSET)) + padding[BitmapFontMetaData.PADDING_TOP_INDEX]) * yScreenSpaceConversion;
				quadWidth = width * xScreenSpaceConversion;
				quadHeight = height * yScreenSpaceConversion;
				xAdvance = (Integer.parseInt(lineData.get(BitmapFontMetaData.TAG_XADVANCE)) + paddingWidth) * xScreenSpaceConversion;

				character = new BitmapGlyph(id, xTex, yTex, xTexSize, yTexSize, xOff, yOff, quadWidth, quadHeight, xAdvance);
				characters.add(character);
			}
		}
		stream.close();
		reader.close();

		controlData = new BitmapFontMetaData(lineData);
		return new BitmapTypeFace(controlData, bitmap, characters);
	}

	public static RawModel loadResourceAsRawModel(String objPath, ModelLoader3D modelLoader) throws IOException
	{
		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Vector2f> textureCoords = new ArrayList<Vector2f>();
		List<Integer> indices = new ArrayList<Integer>();

		float[] verticesArray = null;
		float[] textureCoordsArray = null;
		float[] normalsArray = null;
		int[] indicesArray = null;

		InputStream stream = FileUtil.loadResourceAsStream(objPath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

		String line;
		while((line = reader.readLine()) != null)
		{
			if(line.startsWith("v "))
			{
				float x = Float.valueOf(line.split(" ")[1]);
				float y = Float.valueOf(line.split(" ")[2]);
				float z = Float.valueOf(line.split(" ")[3]);

				vertices.add(new Vector3f(x, y, z));
			}
			else if(line.startsWith("vn "))
			{
				float x = Float.valueOf(line.split(" ")[1]);
				float y = Float.valueOf(line.split(" ")[2]);
				float z = Float.valueOf(line.split(" ")[3]);

				normals.add(new Vector3f(x, y, z));
			}
			else if(line.startsWith("vt "))
			{
				float x = Float.valueOf(line.split(" ")[1]);
				float y = Float.valueOf(line.split(" ")[2]);

				textureCoords.add(new Vector2f(x, 1.0f - y));
			}
			else if(line.startsWith("f "))
			{
				textureCoordsArray = new float[vertices.size() * 2];
				normalsArray = new float[vertices.size() * 3];
				break;
			}
		}

		Integer index0 = null, index1 = null, index2 = null;

		while(line != null)
		{
			if(line.startsWith("f "))
			{
				String[] currLine = line.split(" ");
				String[] v0 = currLine[1].split("/");
				String[] v1 = currLine[2].split("/");
				String[] v2 = currLine[3].split("/");

				index0 = Integer.parseInt(v0[0]) - 1;
				index1 = Integer.parseInt(v1[0]) - 1;
				index2 = Integer.parseInt(v2[0]) - 1;

				indices.add(index2);
				indices.add(index1);
				indices.add(index0);

				processModelVertex(v2, textureCoords, normals, textureCoordsArray, normalsArray);
				processModelVertex(v1, textureCoords, normals, textureCoordsArray, normalsArray);
				processModelVertex(v0, textureCoords, normals, textureCoordsArray, normalsArray);
			}

			line = reader.readLine();
		}
		reader.close();

		verticesArray = new float[vertices.size() * 3];
		indicesArray = new int[indices.size()];

		int vertexPointer = 0;

		for(Vector3f vertex : vertices)
		{
			verticesArray[vertexPointer++] = vertex.x;
			verticesArray[vertexPointer++] = vertex.y;
			verticesArray[vertexPointer++] = vertex.z;
		}
		for(int i = 0; i < indices.size(); i++)
		{
			indicesArray[i] = indices.get(i);
		}

		return modelLoader.loadToVAO(verticesArray, textureCoordsArray, normalsArray, indicesArray);
	}

	private static void processModelVertex(String[] vertexData, List<Vector2f> textureCoords, List<Vector3f> normals, float[] textureCoordsArray, float[] normalsArray)
	{
		int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;

		Vector2f currentTextureCoord = textureCoords.get(Integer.parseInt(vertexData[1]) - 1);
		textureCoordsArray[currentVertexPointer * 2] = currentTextureCoord.x;
		textureCoordsArray[(currentVertexPointer * 2) + 1] = currentTextureCoord.y;

		Vector3f currentNormal = normals.get(Integer.parseInt(vertexData[2]) - 1);
		normalsArray[currentVertexPointer * 3] = currentNormal.x;
		normalsArray[(currentVertexPointer * 3) + 1] = currentNormal.y;
		normalsArray[(currentVertexPointer * 3) + 2] = currentNormal.z;
	}

	public static RawImage loadResourceAsRawImage(String imagePath) throws IOException
	{
		InputStream stream = FileUtil.loadResourceAsStream(imagePath);

		BufferedImage image = ImageIO.read(stream);

		int width = image.getWidth();
		int height = image.getHeight();

		Color[][] pixels = new Color[width][height];

		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				pixels[x][y] = new Color(image.getRGB(x, y));
			}
		}

		return new RawImage(pixels);
	}

	public static HeightmapModel loadResourceAsHeightmapModel(String mapPath, Vector3f scale, ModelTexture[] modelTextures, ModelLoader3D modelLoader) throws IOException
	{
		InputStream stream = FileUtil.loadResourceAsStream(mapPath);

		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Vector2f> textureCoords = new ArrayList<Vector2f>();
		List<Integer> indices = new ArrayList<Integer>();

		float[] verticesArray = null;
		float[] textureCoordsArray = null;
		float[] normalsArray = null;
		int[] indicesArray = null;

		BufferedImage image = ImageIO.read(stream);

		int width = image.getWidth();
		int height = image.getHeight();

		float[][] heightData = new float[width][height];

		Color color;

		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				color = new Color(image.getRGB(x, height - 1 - y));

				heightData[x][y] = color.getRed();
			}
		}

		float texCoordX, texCoordY;
		for(int y = height - 1; y >= 0; y--)
		{
			for(int x = 0; x < width; x++)
			{
				if(y < height)
				{
					Vector3f verts = new Vector3f(x * scale.x, heightData[x][y] * scale.y, y * scale.z);
					Vector3f norms = verts.normalise(new Vector3f());

					texCoordX = x / (float) (width - 1);
					texCoordY = (height - 1 - y) / (float) (height - 1);

					vertices.add(verts);
					normals.add(norms);
					textureCoords.add(new Vector2f(texCoordX, texCoordY));
				}
			}
		}

		int tile = 0;
		int A, B, C; //Top-left, Bottom-left, Top-right
		int D, E, F; //Top-right, Bottom-left, Bottom-right

		//See notebook for how this works
		for(int step = 0; step < Math.pow(width, 2) - width; step++)
		{
			A = step;
			B = A + width;
			C = A + 1;
			D = C;
			E = B;
			F = B + 1;

			indices.add(A);
			indices.add(B);
			indices.add(C);
			indices.add(D);
			indices.add(E);
			indices.add(F);

			tile++;

			if(tile >= width - 1)
			{
				tile = 0;
				step++;
			}
		}

		indicesArray = new int[indices.size()];
		verticesArray = new float[vertices.size() * 3];
		normalsArray = new float[normals.size() * 3];

		int vertexPointer = 0;
		int normalPointer = 0;
		int texturePointer = 0;

		for(Vector3f vertex : vertices)
		{
			verticesArray[vertexPointer++] = vertex.x;
			verticesArray[vertexPointer++] = vertex.y;
			verticesArray[vertexPointer++] = vertex.z;
		}
		for(Vector3f normal : normals)
		{
			normalsArray[normalPointer++] = normal.x;
			normalsArray[normalPointer++] = normal.y;
			normalsArray[normalPointer++] = normal.z;
		}
		textureCoordsArray = new float[textureCoords.size() * 2];
		for(Vector2f textureCoord : textureCoords)
		{
			textureCoordsArray[texturePointer++] = textureCoord.x;
			textureCoordsArray[texturePointer++] = textureCoord.y;
		}
		for(int i = 0; i < indices.size(); i++)
		{
			indicesArray[i] = indices.get(i);
		}

		RawModel rawModel = modelLoader.loadToVAO(verticesArray, textureCoordsArray, normalsArray, indicesArray);

		return new HeightmapModel(rawModel, modelTextures, heightData, new Vector2f(scale.x, scale.z));
	}
}