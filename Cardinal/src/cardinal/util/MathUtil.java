package cardinal.util;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class MathUtil
{
	public static final double EPSILON = 0.00001;

	public static double getPositiveAngle(double angleInDegrees)
	{
			
		return (angleInDegrees < 0.0) ? (360.0 - (Math.abs(angleInDegrees % 360.0))) : (angleInDegrees % 360.0);
	}
	
	public static double getOppositeAngle(double angleInDegrees)
	{
		double oppositeAngle;
		angleInDegrees = cycle(angleInDegrees, 0.0, 360.0, false);
		if(angleInDegrees < 0.0)
		{
			angleInDegrees %= 360.0;
			angleInDegrees += 360.0;
		}
		else if(angleInDegrees > 360.0)
			angleInDegrees %= 360.0;

		if(angleInDegrees > 180.0)
			oppositeAngle = (180.0 + angleInDegrees) % 360.0;
		else
			oppositeAngle = 360.0 - angleInDegrees;

		return Math.toDegrees(oppositeAngle); //TODO invert snellsLaw angle
	}

	public static double snellsLaw(float lightRayAngle, float refractionIndex)
	{
		double sinTheta0 = Math.sin(Math.toRadians(lightRayAngle));
		double theta1 = Math.asin(sinTheta0 / refractionIndex);

		return Math.toDegrees(theta1);
	}

	public static boolean inRange(double f0, double minValue, double maxValue)
	{
		return (f0 >= minValue && f0 <= maxValue);
	}

	public static double cycle(double numToCycle, double origin, double maxDistance, boolean isOriginLowerLimit)
	{
		double numToReturn = numToCycle;

		double lowAdd = (isOriginLowerLimit) ? origin + maxDistance : origin;
		double highAdd = origin;

		double maxValue = origin + maxDistance;
		double minValue = isOriginLowerLimit ? origin : origin - maxDistance;

		if(numToCycle < minValue)
			numToReturn = lowAdd + (numToCycle % maxDistance);
		if(numToCycle > maxValue)
			numToReturn = highAdd + (numToCycle % maxDistance);

		return numToReturn;
	}

	public static double limit(double numToLimit, double lowerLimit, double upperLimit)
	{
		double numToReturn = numToLimit;

		if(numToReturn < lowerLimit)
			numToReturn = lowerLimit;
		else if(numToReturn > upperLimit)
			numToReturn = upperLimit;

		return numToReturn;
	}

	public static boolean areEqual(double f0, double f1)
	{
		return f0 == f1 || Math.abs(f0 - f1) < EPSILON;
	}

	public static boolean greaterThan(double f0, double f1)
	{
		return f0 - f1 > EPSILON;
	}

	public static boolean lessThan(double f0, double f1)
	{
		return f1 - f0 > EPSILON;
	}

	public static boolean greaterOrEqualTo(double f0, double f1)
	{
		return greaterThan(f0, f1) || areEqual(f0, f1);
	}

	public static boolean lessOrEqualTo(double f0, double f1)
	{
		return lessThan(f0, f1) || areEqual(f0, f1);
	}
}
