package cardinal.util;

import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class VectorUtil
{
	public static Vector3f abs(Vector3f vec)
	{
		return new Vector3f(Math.abs(vec.x), Math.abs(vec.y), Math.abs(vec.z));
	}

	public static void cycleVector(Vector3f vec, float origin, float maxDistance, boolean isOriginLowerLimit)
	{
		vec.x = (float) MathUtil.cycle(vec.x, origin, maxDistance, isOriginLowerLimit);
		vec.y = (float) MathUtil.cycle(vec.y, origin, maxDistance, isOriginLowerLimit);
		vec.z = (float) MathUtil.cycle(vec.z, origin, maxDistance, isOriginLowerLimit);
	}
	
	public static void limitVector(Vector3f vec, Vector3f vecMinima, Vector3f vecMaxima)
	{
		vec.x = (float) MathUtil.limit(vec.x, vecMinima.x, vecMaxima.x);
		vec.y = (float) MathUtil.limit(vec.y, vecMinima.y, vecMaxima.y);
		vec.z = (float) MathUtil.limit(vec.z, vecMinima.z, vecMaxima.z);
	}

	public static Vector3f calculateVertexNormals(List<Vector3f> vertices, int width, int height)
	{
		//TODO
		return null;
	}

	public static Vector3f calculateFaceNormal(Vector3f v0, Vector3f v1, Vector3f v2)
	{
		Vector3f V0 = new Vector3f();
		Vector3f V1 = new Vector3f();
		Vector3f normal = new Vector3f();

		Vector3f.sub(v0, v1, V0);
		Vector3f.sub(v2, v0, V1);
		Vector3f.cross(V0, V1, normal).normalise();

		if(normal.x != 0.0f)
			normal.x *= -1.0f;
		if(normal.y != 0.0f)
			normal.y *= -1.0f;
		if(normal.z != 0.0f)
			normal.z *= -1.0f;

		return (Vector3f) normal;
	}

	public static Vector3f getMidpoint(Vector3f v0, Vector3f v1)
	{
		Vector3f midPoint = new Vector3f();
		midPoint.x = (v0.x + v1.x) / 2.0f;
		midPoint.y = (v0.y + v1.y) / 2.0f;
		midPoint.z = (v0.z + v1.z) / 2.0f;

		return midPoint;
	}

	public static Vector3f getMidpoint(Vector3f v0, Vector3f v1, Vector3f v2)
	{
		Vector3f midPoint = new Vector3f();
		midPoint.x = (v0.x + v1.x + v2.x) / 3.0f;
		midPoint.y = (v0.y + v1.y + v2.y) / 3.0f;
		midPoint.z = (v0.z + v1.z + v2.z) / 3.0f;

		return midPoint;
	}

	public static Vector3f vectorToRadians(Vector3f vec3)
	{
		Vector3f inRadians = new Vector3f(0.0f, 0.0f, 0.0f);

		inRadians.x = (float) Math.toRadians(vec3.x);
		inRadians.y = (float) Math.toRadians(vec3.y);
		inRadians.z = (float) Math.toRadians(vec3.z);

		return inRadians;
	}

	public static float interpolate(Vector2f vert0, Vector2f vert1, float position)
	{
		float slope = (vert0.y - vert1.y) / (vert0.x - vert1.x);

		return slope * position;
	}

	public static float interpolateBaryCentric(Vector3f vert0, Vector3f vert1, Vector3f vert2, Vector2f position)
	{
		float determinant = (vert1.z - vert2.z) * (vert0.x - vert2.x) + (vert2.x - vert1.x) * (vert0.z - vert2.z);
		float u0 = ((vert1.z - vert2.z) * (position.x - vert2.x) + (vert2.x - vert1.x) * (position.y - vert2.z)) / determinant;
		float u1 = ((vert2.z - vert0.z) * (position.x - vert2.x) + (vert0.x - vert2.x) * (position.y - vert2.z)) / determinant;
		float u2 = 1.0f - u0 - u1;
		return u0 * vert0.y + u1 * vert1.y + u2 * vert2.y;
	}

	public static Vector4f cloneVector(Vector4f vec)
	{
		return new Vector4f(vec.x, vec.y, vec.z, vec.w);
	}

	public static Vector3f cloneVector(Vector3f vec)
	{
		return new Vector3f(vec.x, vec.y, vec.z);
	}

	public static Vector2f cloneVector(Vector2f vec)
	{
		return new Vector2f(vec.x, vec.y);
	}

	public static boolean compareVectors(Vector3f v0, Vector3f v1)
	{
		return (v0.x == v1.x && v0.y == v1.y && v0.z == v1.z);
	}

	public static float[] asFloats(Vector2f vector)
	{
		return new float[]
		{ vector.x, vector.y };
	}

	public static float[] asFloats(Vector3f vector)
	{
		return new float[]
		{ vector.x, vector.y, vector.z };
	}

	public static float[] asFloats(Vector4f vector)
	{
		return new float[]
		{ vector.x, vector.y, vector.z, vector.w };
	}
}
