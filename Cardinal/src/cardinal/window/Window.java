package cardinal.window;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import cardinal.annotation.Preamble;
import cardinal.controls.Controller;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Window
{
	private static int WIDTH, HEIGHT;
	private static int SYNC;
	
	private static String TITLE;
	
	private static boolean FULLSCREEN;
	private static boolean RESIZABLE;
	
	private static Timer TIMER;
	private static int DELTA;
	
	private static boolean CLOSES_ON_KEYPRESS;
	private static int CLOSE_KEY;
	private static boolean KEY_CLOSE_REQUESTED;
	
	public static void createWindow(int width, int height, String title)
	{
		try
		{
			TIMER = new Timer();
			WIDTH = width;
			HEIGHT = height;
			TITLE = title;
			FULLSCREEN = false;
			SYNC = 0;
			RESIZABLE = false;
			CLOSES_ON_KEYPRESS = false;
			KEY_CLOSE_REQUESTED = false;
			
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setTitle(TITLE);
			Display.setResizable(RESIZABLE);
			Display.create();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void update()
	{
		TIMER.updateFPS();
		DELTA = TIMER.getDelta();
		
		Controller.update();
		if(CLOSES_ON_KEYPRESS  && Controller.isKeyPressed(CLOSE_KEY)) 
			KEY_CLOSE_REQUESTED = true;
		Display.sync(SYNC);
		
		Display.update();
	}
	
	public static void destroy()
	{
		Controller.grabMouse(false);
		Display.destroy();
	}
	
	public static void setCloseOnKeyPress(int controllerKey, boolean closeOnKeyPress)
	{
		CLOSES_ON_KEYPRESS = closeOnKeyPress;
		CLOSE_KEY = controllerKey;
	}
	
	public static boolean isCloseOnKeyPressEnabled()
	{
		return CLOSES_ON_KEYPRESS;
	}
	
	public static boolean isCloseRequested()
	{
		return Display.isCloseRequested() || KEY_CLOSE_REQUESTED;
	}
	
	public static void setResizable(boolean resizable)
	{
		RESIZABLE = resizable;
		Display.setResizable(RESIZABLE);
	}
	
	public static boolean getResizable()
	{
		return RESIZABLE;
	}
	
	public static void setSync(int sync)
	{
		SYNC = sync;
	}
	
	public static int getSync()
	{
		return SYNC;
	}
	
	public static void setFullscreen(boolean fullscreen)
	{
		try
		{
			FULLSCREEN = fullscreen;
			Display.setFullscreen(FULLSCREEN);
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static boolean getFullscreen()
	{
		return FULLSCREEN;
	}
	
	public static void setWidth(int width)
	{
		WIDTH = width;
		resetDisplayMode();
	}
	
	public static int getWidth()
	{
		return WIDTH;
	}

	public static void setHeight(int height)
	{
		HEIGHT = height;
		resetDisplayMode();
	}
	
	public static int getHeight()
	{
		return HEIGHT;
	}

	public static void setTitle(String title)
	{
		TITLE = title;
		Display.setTitle(title);
	}
	
	public static String getTitle()
	{
		return TITLE;
	}
	
	public static float getAspect()
	{
		return (float) (Display.getWidth() / (float) Display.getHeight());
	}
	
	public static int getFPS()
	{
		return TIMER.getFPS();
	}
	
	public static int getDelta()
	{
		return DELTA;
	}
	
	private static void resetDisplayMode()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}
}

class Timer
{
	long lastFrame;

	int fps;
	long lastFPS;
	int fpsToReturn;
	
	int delta;
	
	public Timer()
	{
		lastFPS = getTime();
	}

	public long getTime()
	{
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	public int getDelta()
	{
		long time = getTime();
		delta = (int) (time - lastFrame);
		lastFrame = time;

		return delta;
	}

	public void updateFPS()
	{
		if (getTime() - lastFPS > 1000)
		{	
			fpsToReturn = fps;
			
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}

	public int getFPS()
	{
		return fpsToReturn;
	}
}

