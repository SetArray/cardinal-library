package cardinal.graphics;

import java.awt.Color;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RawImage
{
	private Color[][] pixels;
	private int width, height;
	
	public RawImage(Color[][] pixels)
	{
		this.pixels = pixels;
		this.width = pixels[0].length;
		this.height = pixels[1].length;
	}
	
	public Color getPixel(int x, int y)
	{
		return pixels[x][y];
	}
	
	public Color[][] getPixels()
	{
		return pixels;
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
}
