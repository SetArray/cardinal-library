package cardinal.graphics.g3d;

import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class ModelTexture
{	
	private int texture;
	private float opacity;
	
	private int numberOfRows;
	

	public ModelTexture(int texture)
	{
		this(texture, 1.0f, 1);
	}
	
	public ModelTexture(int texture, float opacity)
	{
		this(texture, opacity, 1);
	}
	
	public ModelTexture(int texture, float opacity, int numberOfRows)
	{
		this.texture = texture;
		this.opacity = opacity;
		this.numberOfRows = 1;
	}
	
	public void setTexture(int texture)
	{
		this.texture = texture;
	}
	
	public int getTexture()
	{
		return texture;
	}
	
	public void setOpacity(float opacity)
	{
		this.opacity = opacity;
	}
	
	public float getOpacity()
	{
		return opacity;
	}
	
	public void setNumberOfRows(int numberOfRows)
	{
		this.numberOfRows = numberOfRows;
	}
	
	public int getNumberOfRows()
	{
		return numberOfRows;
	}
	
	/**
	 * @param index index of texture in the atlas
	 * @return Texture coords offset for textures in an atlas
	 */
	public Vector2f getAtlasIndexOffset(int index)
	{
		return new Vector2f(((float) (index % numberOfRows) / numberOfRows), ((float) (index / numberOfRows) / numberOfRows));
	}
}

