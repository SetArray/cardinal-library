package cardinal.graphics.g3d;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.controls.Controller;
import cardinal.util.MathUtil;
import cardinal.util.VectorUtil;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Camera
{
	private Matrix4f viewMatrix;
	
	private Vector3f viewMatrixPosition, viewMatrixRotation;
	private Vector3f position, rotation;
	
	private Vector3f rotationMinima;
	private Vector3f rotationMaxima;
	private boolean restrictRotation;

	public Camera()
	{
		this(new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f));
	}
	
	public Camera(Vector3f position, Vector3f rotation)
	{
		this.position = position;
		this.rotation = rotation;

		this.rotationMinima = new Vector3f();
		this.rotationMaxima = new Vector3f();
		this.restrictRotation = false;
		
		viewMatrix = new Matrix4f();
		setViewMatrix();
	}
	
	public void update()
	{
		processInput();
		if(restrictRotation)
			VectorUtil.limitVector(rotation, rotationMinima, rotationMaxima);
		VectorUtil.cycleVector(rotation, 0.0f, 360.0f, false);
		
		setViewMatrix();
	}
	
	private static float SPEED = 0.3f;
	
	private void processInput()
	{
		float calculatedSpeed = SPEED * Window.getDelta();
		float xMovementWS = (float) (Math.sin(Math.toRadians(rotation.y)) * calculatedSpeed);
		float zMovementWS = (float) (Math.cos(Math.toRadians(rotation.y)) * calculatedSpeed);
		float xMovemenAD = (float) (Math.sin(Math.toRadians(rotation.y - 90)) * calculatedSpeed);
		float zMovementAD = (float) (Math.cos(Math.toRadians(rotation.y - 90)) * calculatedSpeed);
		
		if(Controller.isKeyDown(Controller.KEY_W))
		{
			position.x -= xMovementWS;
			position.z += zMovementWS;;
		}
		if(Controller.isKeyDown(Controller.KEY_S))
		{
			position.x += xMovementWS;
			position.z -= zMovementWS;
		}
		if(Controller.isKeyDown(Controller.KEY_A))
		{
			
			position.x += xMovemenAD;
			position.z -= zMovementAD;
		}
		if(Controller.isKeyDown(Controller.KEY_D))
		{
			position.x -= xMovemenAD;
			position.z += zMovementAD;
		}
		if(Controller.isKeyDown(Controller.KEY_LSHIFT))
		{
			position.y -= 0.4f * calculatedSpeed;
		}
		if(Controller.isKeyDown(Controller.KEY_SPACE))
		{
			position.y += 0.4f * calculatedSpeed;
		}

		if(Controller.getMouseDX() != 0)
			rotation.y -= (Controller.getMouseDX()/1.5f);
		if(Controller.getMouseDY() != 0)
			rotation.x += (Controller.getMouseDY()/1.5f);
	}

	private void setViewMatrix()
	{
		viewMatrixPosition = new Vector3f(position);
		viewMatrixRotation = new Vector3f(rotation);
		
		viewMatrix.setIdentity();
		Matrix4f.rotate((float) Math.toRadians(-rotation.x), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
		Matrix4f.rotate((float) Math.toRadians(-rotation.y), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
		Matrix4f.rotate((float) Math.toRadians(-rotation.z + 180), new Vector3f(0, 0, 1), viewMatrix, viewMatrix);
		Matrix4f.translate(viewMatrixPosition, viewMatrix, viewMatrix);
		Matrix4f.scale(new Vector3f(-1.0f, -1.0f, 1.0f), viewMatrix, viewMatrix);
	}
	
	public Matrix4f getViewMatrix()
	{
		if(isViewMatrixOutdated())
			setViewMatrix();
		
		return viewMatrix;
	}
	
	public boolean isViewMatrixOutdated()
	{
		if(VectorUtil.compareVectors(position, viewMatrixPosition) && VectorUtil.compareVectors(rotation, viewMatrixRotation))
			return false;
		
		return true;
	}
	
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	
	public Vector3f getPosition()
	{
		return position;
	}
	
	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}
	
	public Vector3f getRotation()
	{
		return rotation;
	}
	
	public void setRotationRestrictions(Vector3f rotationMinima, Vector3f rotationMaxima)
	{
		this.rotationMinima = rotationMinima;
		this.rotationMaxima = rotationMaxima;
	}

	public void setRotationMinima(Vector3f rotationMinima)
	{
		this.rotationMinima = rotationMinima;
	}

	public void setRotationMaxima(Vector3f rotationMaxima)
	{
		this.rotationMaxima = rotationMaxima;
	}

	public Vector3f getRotationMinima()
	{
		return rotationMinima;
	}

	public Vector3f getRotationMaxima()
	{
		return rotationMaxima;
	}

	public boolean isRotationRestricted()
	{
		return restrictRotation;
	}

	public void restrictRotation(boolean restrictRotation)
	{
		this.restrictRotation = restrictRotation;
	}
}
