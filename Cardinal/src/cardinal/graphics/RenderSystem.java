package cardinal.graphics;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;
import cardinal.componentArch.events.RenderEvent;
import cardinal.componentArch.g3d.TransformComponent;
import cardinal.graphics.g3d.Camera;
import cardinal.shader.ShaderProgram;
import cardinal.shader.TextShader;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RenderSystem
{
	private float fov, zNear, zFar;

	private Matrix4f projectionMatrix;
	private Matrix4f orthogonalMatrix;

	public RenderSystem(float fov, float zNear, float zFar)
	{
		this.fov = fov;
		this.zNear = zNear;
		this.zFar = zFar;

		enableGL();

		createProjectionMatrix();
		createOrthogonalMatrix(0, Display.getWidth(), Display.getHeight(), 0, -1.0f, 1.0f);
		createOrthogonalProjectionMatrix();
	}

	public void createProjectionMatrix()
	{
		float yScale = 1.0f / (float) Math.tan(Math.toRadians(fov / 2.0f));
		float xScale = yScale / Window.getAspect();
		float frustrumLength = zFar - zNear;

		projectionMatrix = new Matrix4f();
		projectionMatrix.setIdentity();

		projectionMatrix.m00 = xScale;
		projectionMatrix.m11 = yScale;
		projectionMatrix.m22 = -((zFar + zNear) / frustrumLength);
		projectionMatrix.m23 = -1.0f;
		projectionMatrix.m32 = -((2.0f * zNear * zFar) / frustrumLength);
		projectionMatrix.m33 = 0.0f;
	}

	public void createOrthogonalMatrix(float left, float right, float bottom, float top, float near, float far)
	{
		float x_orth = -2.0f / (right - left);
		float y_orth = -2.0f / (top - bottom);
		float z_orth = -2.0f / (far - near);

		float tx = -(right + left) / (right - left);
		float ty = -(top + bottom) / (top - bottom);
		float tz = -(far + near) / (far - near);

		orthogonalMatrix = new Matrix4f();
		orthogonalMatrix.setIdentity();

		orthogonalMatrix.m00 = x_orth;
		orthogonalMatrix.m10 = 0;
		orthogonalMatrix.m20 = 0;
		orthogonalMatrix.m30 = 0;
		orthogonalMatrix.m01 = 0;
		orthogonalMatrix.m11 = y_orth;
		orthogonalMatrix.m21 = 0;
		orthogonalMatrix.m31 = 0;
		orthogonalMatrix.m02 = 0;
		orthogonalMatrix.m12 = 0;
		orthogonalMatrix.m22 = z_orth;
		orthogonalMatrix.m32 = 0;
		orthogonalMatrix.m03 = tx;
		orthogonalMatrix.m13 = ty;
		orthogonalMatrix.m23 = tz;
		orthogonalMatrix.m33 = 1.0f;
		orthogonalMatrix = Matrix4f.scale(new Vector3f(1.0f, 1.0f, -1.0f), orthogonalMatrix, null);
	}

	public void createOrthogonalProjectionMatrix()
	{
		Matrix4f.mul(projectionMatrix, orthogonalMatrix, null);
	}

	public void render(ShaderProgram shader, List<GameObject> gameObjects, Camera camera, Object... extraUniforms)
	{
		shader.start();
		for(GameObject gameObject : gameObjects)
		{
			shader.loadShaderData(
					(shader instanceof TextShader) ? Matrix4f.mul(projectionMatrix, orthogonalMatrix, null) : projectionMatrix, 
							camera.getViewMatrix(), 
							((TransformComponent) gameObject.getComponent(TransformComponent.getStaticID())).getModelMatrix(), 
							gameObject, 
							extraUniforms
					);

			gameObject.fireEvent(new RenderEvent());
		}
		shader.stop();
	}

	public void render(ShaderProgram shader, GameObject gameObject, Camera camera, Object... extraUniforms)
	{
		shader.start();
		shader.loadShaderData(projectionMatrix, camera.getViewMatrix(), ((TransformComponent) gameObject.getComponent(TransformComponent.getStaticID())).getModelMatrix(), gameObject, extraUniforms);
		gameObject.fireEvent(new RenderEvent());
		shader.stop();
	}

	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}

	public Matrix4f getOrthogonalMatrix()
	{
		return orthogonalMatrix;
	}

	public static void enableGL()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public static void enableGL2D()
	{
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL30.GL_CLIP_DISTANCE0);
	}

	public static void enableGL3D()
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glEnable(GL30.GL_CLIP_DISTANCE0);
	}

	public static void clearColorBufferBit()
	{
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	}

	public static void clearDepthBufferBit()
	{
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
	}
}
