package cardinal.graphics.model.g3d;

import org.lwjgl.opengl.GL30;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RawModel
{
	private int vaoID;
	private int vertexCount;
	private int numVertexAttributes;
	
	public RawModel(int vaoID, int vertexCount, int numVertexAttributes)
	{
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
		this.numVertexAttributes = numVertexAttributes;
	}
	
	public int getVAOID()
	{
		return vaoID;
	}
	
	public int getVertexCount()
	{
		return vertexCount;
	}
	
	public void destroy()
	{
		GL30.glDeleteVertexArrays(vaoID);
	}
	
	public int getVertexAttributes()
	{
		return numVertexAttributes;
	}
}
