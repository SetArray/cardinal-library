package cardinal.graphics.model.g3d;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.graphics.g3d.ModelTexture;
import cardinal.util.FloatUtil;
import cardinal.util.VectorUtil;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class HeightmapModel extends TexturedModel
{
	private float[][] heightData;
	private Vector2f scale;

	public HeightmapModel(RawModel model, ModelTexture[] modelTextures, float[][] data, Vector2f scale)
	{
		super(model, modelTextures);
		this.heightData = data;
		this.scale = scale;
	}

	public float[][] getHeightData()
	{
		return FloatUtil.cloneFloatArray(heightData);
	}

	public float getModelWidth()
	{
		return heightData[0].length * scale.x;
	}

	public float getModelLength()
	{
		return heightData[1].length * scale.y;
	}

	public int getImageWidth()
	{
		return heightData[0].length;
	}

	public int getImageHeight()
	{
		return heightData[1].length;
	}

	public Vector2f getScale()
	{
		return scale;
	}
	
	/**
	 * @param mapPosition position relative to map origin
	 * @return tile of the heightmap the coordinate is located in
	 */
	public Vector2f getTilePosition(Vector2f mapPosition)
	{
		Vector2f tilePosition = new Vector2f();
		tilePosition.x = (float) Math.floor(mapPosition.x);
		tilePosition.y = (float) Math.floor(mapPosition.y);

		return tilePosition;
	}

	/**
	 * @param worldPosition position relative to world origin
	 * @return tile of heightmap the coordinate is located in
	 */
	public Vector2f getTilePosition(Vector3f worldPosition)
	{
		return getTilePosition(getMapPosition(worldPosition));
	}

	/**
	 * @param mapCoords position on heightmap
	 * @return position relative to world origin
	 * @throws IndexOutOfBoundsException
	 */
	public Vector3f getWorldPosition(Vector2f mapCoords) throws IndexOutOfBoundsException
	{
		Vector3f worldPosition = new Vector3f();
		
		worldPosition.x = mapCoords.x * scale.x;
		worldPosition.y = getHeightAtMapPosition(mapCoords);
		worldPosition.z = mapCoords.y * scale.y;

		return worldPosition;
	}
	
	/**
	 * @param worldPosition position relative to world origin
	 * @return position relative to map origin
	 */
	public Vector2f getMapPosition(Vector3f worldPosition)
	{
		Vector2f mapPosition = new Vector2f();
		
		mapPosition.x = worldPosition.x / scale.x;
		mapPosition.y = (worldPosition.z + 0.0f) / scale.y;

		return mapPosition;
	}

	/**
	 * @param worldPosition position relative to world origin
	 * @return height of terrain relative to world origin at given position
	 */
	public float getHeightAtWorldPosition(Vector3f worldPosition)
	{
		return getWorldPosition(getMapPosition(worldPosition)).y;
	}

	/**
	 * @param mapCoords position relative to map origin
	 * @return height of terrain relative to world origin at given position
	 */
	public float getHeightAtMapPosition(Vector2f mapCoords)
	{
		float height = 0.0f;

		Vector2f tileCoords = getTilePosition(mapCoords);
		int tileCoordX = (int) tileCoords.x;
		int tileCoordY = (int) tileCoords.y;

		if(isMapPositionInBounds(mapCoords))
		{
			Vector2f gridCoords = new Vector2f(mapCoords.x % 1.0f, mapCoords.y % 1.0f);

			Vector3f v0 = new Vector3f();
			Vector3f v1 = new Vector3f();
			Vector3f v2 = new Vector3f();
			
			if(gridCoords.x <= gridCoords.y) //if (in left triangle of tile)
			{
				v0 = new Vector3f(0.0f, heightData[tileCoordX][tileCoordY + 1], 1.0f);
				v1 = new Vector3f(0.0f, heightData[tileCoordX][tileCoordY], 0.0f);
				v2 = new Vector3f(1.0f, heightData[tileCoordX + 1][tileCoordY + 1], 1.0f);
			}
			else
			{
				v0 = new Vector3f(1.0f, heightData[tileCoordX + 1][tileCoordY + 1], 1.0f);
				v1 = new Vector3f(0.0f, heightData[tileCoordX][tileCoordY], 0.0f);
				v2 = new Vector3f(1.0f, heightData[tileCoordX + 1][tileCoordY], 0.0f);
			}

			height = VectorUtil.interpolateBaryCentric(v0, v1, v2, gridCoords);
		}
		
		return height;
	}

	/**
	 * @param worldPosition position relative to world origin
	 * @return {@code true} if position is located within the terrain
	 */
	public boolean isWorldPositionInBounds(Vector3f worldPosition)
	{
		return isMapPositionInBounds(getMapPosition(worldPosition));
	}

	/**
	 * @param worldPosition position relative to map origin
	 * @return true if position is located within the terrain
	 */
	public boolean isMapPositionInBounds(Vector2f mapPosition)
	{
		boolean xInBounds = mapPosition.x >= 0.0f && mapPosition.x < getImageWidth() - 1;
		boolean yInBounds = mapPosition.y >= 0.0f && mapPosition.y < getImageHeight() - 1;

		return (xInBounds && yInBounds);
	}
}
