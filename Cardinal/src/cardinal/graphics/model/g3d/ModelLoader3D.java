package cardinal.graphics.model.g3d;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.graphics.model.g2d.ModelLoader2D;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class ModelLoader3D extends ModelLoader2D
{	
	public RawModel createFlatRawModel(Vector3f topLeft, Vector3f botLeft, Vector3f topRight, Vector3f botRight)
	{
		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector2f> texCoords = new ArrayList<Vector2f>();
		List<Integer> indices = new ArrayList<Integer>();

		vertices.add(topLeft);
		vertices.add(botLeft);
		vertices.add(topRight);
		vertices.add(botRight);

		texCoords.add(new Vector2f(0.0f, 0.0f));
		texCoords.add(new Vector2f(0.0f, 1.0f));
		texCoords.add(new Vector2f(1.0f, 0.0f));
		texCoords.add(new Vector2f(1.0f, 1.0f));
		
		indices.add(0);
		indices.add(1);
		indices.add(2);
		
		indices.add(2);
		indices.add(1);
		indices.add(3);	
		
		float[] verticesArray = new float[vertices.size() * 3];
		float[] normalsArray = new float[verticesArray.length];
		float[] texCoordsArray = new float[texCoords.size() * 2];
		int[] indicesArray = new int[indices.size()];

		int vertexPointer = 0;
		int texCoordPointer = 0;

		for(Vector3f vertex : vertices)
		{
			verticesArray[vertexPointer] = vertex.x;
			normalsArray[vertexPointer++] = vertex.x;
			
			verticesArray[vertexPointer] = vertex.y;
			normalsArray[vertexPointer++] = vertex.y;
			
			verticesArray[vertexPointer] = vertex.z;
			normalsArray[vertexPointer++] = vertex.z;
		}
		for(Vector2f texCoord : texCoords)
		{
			texCoordsArray[texCoordPointer++] = texCoord.x;
			texCoordsArray[texCoordPointer++] = texCoord.y;
		}
		for(int i = 0; i < indices.size(); i++)
		{
			indicesArray[i] = indices.get(i);
		}

		return loadToVAO(verticesArray, texCoordsArray, normalsArray, indicesArray);
	}
	
	public RawModel loadToVAO(float[] vertices, float[] textureCoords, float[] normals, int[] indices)
	{
		int vaoID = createVAO();	
		vaos.add(vaoID);
		
		bindIndicesVBO(indices);
		storeDataInAttributeList(0, 3, vertices);
		storeDataInAttributeList(1, 2, textureCoords);
		storeDataInAttributeList(2, 3, normals);
		unbindVAO();

		return new RawModel(vaoID, indices.length, 3);
	}
	
	/*
	public static RawModel createRawModel(int width, int depth, Vector2f scale, ModelLoader3D modelLoader)
	{
		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Vector2f> textureCoords = new ArrayList<Vector2f>();
		List<Integer> indices = new ArrayList<Integer>();

		float[] verticesArray = null;
		float[] textureCoordsArray = null;
		float[] normalsArray = null;
		int[] indicesArray = null;

		int xShiftedY;
		float xx0, yy0;
		for(int y = depth - 1; y >= 0; y--)
		{
			for(int x = 0; x < width; x++)
			{
				Vector3f verts = new Vector3f(x * scale.x, 0.0f, -y * scale.y);
				Vector3f norms = verts.normalise(new Vector3f());

				xx0 = x / (float) (width - 1);
				yy0 = (depth - 1 - y) / (float) (depth - 1);

				vertices.add(verts);
				normals.add(norms);
				textureCoords.add(new Vector2f(xx0, yy0));
			}
		}
		
		int topLeft, botLeft, topRight, botRight;
		
		for(int y = 0; y < depth - 1; y++)
		{
			for(int x = 0; x < width - 1; x++)
			{
				xShiftedY = x + (y * width);

				topLeft = xShiftedY ;
				botLeft = x + ((y + 1) * width);
				topRight = xShiftedY + 1;
				botRight = (x + 1) + ((y + 1) * width);

				indices.add(topLeft);
				indices.add(botLeft);
				indices.add(topRight);
				
				indices.add(topRight);
				indices.add(botLeft);
				indices.add(botRight);
			}
		}
		
		indicesArray = new int[indices.size()];
		verticesArray = new float[vertices.size() * 3];
		normalsArray = new float[normals.size() * 3];

		int vertexPointer = 0;
		int normalPointer = 0;
		int texturePointer = 0;

		for(Vector3f vertex : vertices)
		{
			verticesArray[vertexPointer++] = vertex.x;
			verticesArray[vertexPointer++] = vertex.y;
			verticesArray[vertexPointer++] = vertex.z;
		}
		for(Vector3f normal : normals)
		{
			normalsArray[normalPointer++] = normal.x;
			normalsArray[normalPointer++] = normal.y;
			normalsArray[normalPointer++] = normal.z;
		}
		textureCoordsArray = new float[textureCoords.size() * 2];
		for(Vector2f textureCoord : textureCoords)
		{
			textureCoordsArray[texturePointer++] = textureCoord.x;
			textureCoordsArray[texturePointer++] = textureCoord.y;
		}
		for(int i = 0; i < indices.size(); i++)
		{
			indicesArray[i] = indices.get(i);
		}

		return modelLoader.loadToVAO(verticesArray, textureCoordsArray, normalsArray, indicesArray);
	}
	*/
}
