package cardinal.graphics.model.g3d;

import cardinal.annotation.Preamble;
import cardinal.graphics.g3d.ModelTexture;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TexturedModel extends RawModel
{
	protected ModelTexture[] modelTextures;
	
	public TexturedModel(RawModel model, ModelTexture... modelTextures)
	{
		this(model.getVAOID(), model.getVertexCount(), model.getVertexAttributes(), modelTextures);
	}
	
	public TexturedModel(int vaoID, int vertexCount, int numVertexAttributes, ModelTexture... modelTextures)
	{
		super(vaoID, vertexCount, numVertexAttributes);
		this.modelTextures = modelTextures;
	}
	
	public void setModelTexture(int index, ModelTexture modelTexture)
	{
		this.modelTextures[index] = modelTexture;
	}

	public ModelTexture getModelTexture(int index)
	{
		return modelTextures[index];
	}
	
	public void setModelTextures(ModelTexture[] modelTextures)
	{
		this.modelTextures = modelTextures;
	}
	
	public ModelTexture[] getModelTextures()
	{
		return modelTextures;
	}
	
	public int getTextureCount()
	{
		return modelTextures.length;
	}
}
