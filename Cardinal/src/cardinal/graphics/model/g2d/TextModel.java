package cardinal.graphics.model.g2d;

import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;
import cardinal.graphics.g3d.ModelTexture;
import cardinal.graphics.model.g3d.RawModel;
import cardinal.graphics.model.g3d.TexturedModel;
import cardinal.text.bitmapfont.g2d.BitmapTypeFace;
import cardinal.text.g2d.RawTextData;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TextModel extends TexturedModel
{
	private RawTextData textData;
	private Vector4f color;
	
	public TextModel(RawModel rawModel, RawTextData textData)
	{
		this(rawModel, textData, null);
	}
	
	public TextModel(RawModel rawModel, RawTextData textData, Vector4f color)
	{
		super(rawModel, null);
		this.textData = textData;
		this.color = (color != null) ? color : new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public RawTextData getTextData()
	{
		return textData;
	}
	
	public void setColor(Vector4f color)
	{
		this.color = color;
	}
	
	public Vector4f getColor()
	{
		return color;
	}
}
