package cardinal.graphics.model.g2d;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.Preamble;
import cardinal.graphics.model.g3d.RawModel;
import cardinal.text.g2d.RawTextData;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class ModelLoader2D
{
	protected List<Integer> vaos = new ArrayList<Integer>();
	protected List<Integer> vbos = new ArrayList<Integer>();
	
	public TextModel loadTextModel(RawTextData textData, Vector4f color)
	{
		/*
		float[][] vertexData = textData.getFontType().createVertexData(textData);
		int vaoID = loadToVAO(vertexData[0], vertexData[1]);
		
		RawModel rawModel = new RawModel(vaoID, vertexData[0].length / 2, 2);
		return new TextModel(rawModel, textData, color);*/
		
		return null;
	}

	public int loadToVAO(float[] attrib0, float[] attrib1)
	{
		int vaoID = createVAO();	
		vaos.add(vaoID);
		
		storeDataInAttributeList(0, 2, attrib0);
		storeDataInAttributeList(1, 2, attrib1);
		unbindVAO();
		
		return vaoID;
	}
	
	protected void storeDataInAttributeList(int attribNumber, int coordinateSize, float[] data)
	{
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attribNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	protected void bindIndicesVBO(int[] indices)
	{
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}

	protected int createVAO()
	{
		int vaoID = GL30.glGenVertexArrays();
		vaos.add(vaoID);
		
		GL30.glBindVertexArray(vaoID);
		
		return vaoID;
	}

	protected void unbindVAO()
	{
		GL30.glBindVertexArray(0);
	}

	protected IntBuffer storeDataInIntBuffer(int[] data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();

		return buffer;
	}

	protected FloatBuffer storeDataInFloatBuffer(float[] data)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();

		return buffer;
	}

	public void cleanUp()
	{
		for(int vao : vaos)
			GL30.glDeleteVertexArrays(vao);
		for(int vbo : vbos)
			GL15.glDeleteBuffers(vbo);
	}
}
