package cardinal.text.bitmapfont.g2d;

import java.util.HashMap;
import java.util.Map;

import cardinal.annotation.Description;
import cardinal.annotation.Preamble;

@Preamble
(
		dev="Tyler", 
		created="01-16-17", 
		last_modified="01-16-17"
)
@Description("A type of Font using a Bitmap raster and formatted config file to define glyphs")
public class BitmapFont
{
	private BitmapTypeFace typeFace;
	private int size;
	private Map<Integer, float[][]> charVertexData = new HashMap<>();

	public BitmapFont(BitmapTypeFace typeFace, int size)
	{
		this.typeFace = typeFace;
		this.size = size;

		for(BitmapGlyph glyph : typeFace.getGlyphs())
		{
			createVertexData(glyph);
		}
	}

	private float[][] createVertexData(BitmapGlyph glyph)
	{
		float left = glyph.getXOffset() * size;
		float top = glyph.getYOffset() * size;

		float right = (glyph.getXOffset() + glyph.getQuadWidth()) * size;
		float bottom = (glyph.getYOffset() + glyph.getQuadHeight()) * size;

		left = (2.0f * left) - 1.0f;
		top = (-2.0f * top) + 1.0f;
		right = (2.0f * right) - 1.0f;
		bottom = (-2.0f * bottom) + 1.0f;

		float[] vertices = structureVAOAttributeData(left, top, right, bottom);
		float[] texCoords = structureVAOAttributeData(glyph.getXTextureCoord(), glyph.getYTextureCoord(), glyph.getMaxXTextureCoord(), glyph.getMaxYTextureCoord());

		return new float[][]
		{ vertices, texCoords };
	}

	private static float[] structureVAOAttributeData(float left0, float top0, float right0, float bottom0)
	{
		float left1 = left0;
		float top1 = top0;
		float right1 = right0;
		float bottom1 = bottom0;

		float[] data = new float[12];
		int pointer = 0;

		data[pointer++] = (left1);
		data[pointer++] = (top1);

		data[pointer++] = (left1);
		data[pointer++] = (bottom1);

		data[pointer++] = (right1);
		data[pointer++] = (top1);

		data[pointer++] = (right1);
		data[pointer++] = (top1);

		data[pointer++] = (left1);
		data[pointer++] = (bottom1);

		data[pointer++] = (right1);
		data[pointer++] = (bottom1);

		return data;
	}

	public BitmapTypeFace getTypeFace()
	{
		return typeFace;
	}

	public String getName()
	{
		return typeFace.getName() + " " + size + "pt";
	}

	public int getSize()
	{
		return size;
	}

	protected float[] getVertices(int ascii)
	{
		return charVertexData.get(ascii)[0];
	}

	protected float[] getTextureCoords(int ascii)
	{
		return charVertexData.get(ascii)[1];
	}
}
