package cardinal.text.bitmapfont.g2d;

import java.util.HashMap;
import java.util.Map;

import cardinal.annotation.Preamble;

/**
 * TypeFace MetaData
 */
@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class BitmapFontMetaData
{
	public static final String TAG_INFO = "info"; //Start of info portion of file
	public static final String TAG_FACE = "face"; //Name of the TypeFamily
	public static final String TAG_SIZE = "size"; //Size of font
	public static final String TAG_BOLD = "bold"; //Is bold
	public static final String TAG_ITALIC = "italic"; //Is italic
	public static final String TAG_CHARSET = "charset"; //Unknown
	public static final String TAG_UNICODE = "unicode"; //Unknown
	public static final String TAG_STRETCHH = "stretchH"; //Vertical character stretch
	public static final String TAG_SMOOTH = "smooth"; //Smooth letters
	public static final String TAG_AA = "aa"; //Authors Alteration
	public static final String TAG_PADDING = "padding"; //Padding of characters (up, right, down, left)
	public static final String TAG_SPACING = "spacing"; //Spacing of characters (horizontal, vertical)

	public static final String TAG_COMMON = "common"; //Start of common-into portion of file
	public static final String TAG_LINEHEIGHT = "lineHeight";//Distance between lines in pixels
	public static final String TAG_BASE = "base"; //Number of pixels from top of line to base of character
	public static final String TAG_SCALEW = "scaleW"; //Texture atlas width in pixels
	public static final String TAG_SCALEH = "scaleH"; //Texture atlas height in pixels
	public static final String TAG_PAGES = "pages"; //Number of pages texture atlas contains
	public static final String TAG_PACKED = "packed"; //Unknown

	public static final String TAG_FILE = "file"; //Name of bitmap file

	public static final String TAG_CHARS = "chars"; //Start of chars info portion of file
	public static final String TAG_COUNT = "count"; //Number of chars in .FNT file
	public static final String TAG_CHAR = "char"; //Start of line containing data for a character
	public static final String TAG_ID = "id"; //ASCII ID of character
	public static final String TAG_X = "x"; //X-coordinate in pixels for top-left of character in texture atlas
	public static final String TAG_Y = "y"; //Y-coordinate in pixels for top-left of character in texture atlas
	public static final String TAG_WIDTH = "width"; //Width of character in pixels
	public static final String TAG_HEIGHT = "height"; //Height of character in pixels
	public static final String TAG_XOFFSET = "xoffset"; //X-coordinate offset for proper character alignment
	public static final String TAG_YOFFSET = "yoffset"; //Y-coordinate offset for proper character alignment
	public static final String TAG_XADVANCE = "xadvance"; //Distance in pixels for cursor to advance to render next character
	public static final String TAG_PAGE = "page"; //Unknown
	public static final String TAG_CHNL = "chnl"; //Unknown

	public static final int PADDING_TOP_INDEX = 0;
	public static final int PADDING_LEFT_INDEX = 1;
	public static final int PADDING_BOTTOM_INDEX = 2;
	public static final int PADDING_RIGHT_INDEX = 3;

	private Map<String, String> data = new HashMap<String, String>();
	private String face;
	private String file;
	private String charset;
	
	private boolean bold;
	private boolean italic;
	private boolean unicode;
	private boolean smooth;
	private boolean aa;
	
	private int[] padding;
	private int paddingTop;
	private int paddingLeft;
	private int paddingBottom;
	private int paddingRight;
	private int size;
	private int stretchH;
	private int[] spacing;
	private int lineHeight;
	private int base;
	private int scaleW;
	private int scaleH;
	private int pages;
	private int packed;
	private int count;

	public BitmapFontMetaData(Map<String, String> lineData)
	{
		this.data = lineData;
		loadInstanceVariables();
	}
	
	private void loadInstanceVariables()
	{
		this.face = data.get(TAG_FACE);
		this.file = data.get(TAG_FILE);
		this.charset = data.get(TAG_CHARSET);

		int isValid;
		if((isValid = isBoolean(data.get(TAG_BOLD))) != -1) this.bold = isValid != 0;
		if((isValid = isBoolean(data.get(TAG_ITALIC))) != -1) this.italic = isValid != 0;
		if((isValid = isBoolean(data.get(TAG_UNICODE))) != -1) this.unicode = isValid != 0;
		if((isValid = isBoolean(data.get(TAG_SMOOTH))) != -1) this.smooth = isValid != 0;
		if((isValid = isBoolean(data.get(TAG_AA))) != -1) this.aa = isValid != 0;
		
		String[] strPadding = data.get(TAG_PADDING).split(",");
		this.padding = new int[strPadding.length];
		for(int i = 0; i < strPadding.length; i++) padding[i] = Integer.parseInt(strPadding[i]);
		
		this.paddingTop = padding[PADDING_TOP_INDEX];
		this.paddingLeft = padding[PADDING_LEFT_INDEX];
		this.paddingBottom = padding[PADDING_BOTTOM_INDEX];
		this.paddingRight = padding[PADDING_RIGHT_INDEX];
		this.size = Integer.parseInt(data.get(TAG_SIZE));
		this.stretchH = Integer.parseInt(data.get(TAG_STRETCHH));
		String[] strSpacing = data.get(TAG_SPACING).split(",");
		this.spacing = new int[] {Integer.parseInt(strSpacing[0]), Integer.parseInt(strSpacing[1])};
		this.lineHeight = Integer.parseInt(data.get(TAG_LINEHEIGHT));
		this.base = Integer.parseInt(data.get(TAG_BASE));
		this.scaleW = Integer.parseInt(data.get(TAG_SCALEW));
		this.scaleH = Integer.parseInt(data.get(TAG_SCALEH));
		this.packed = Integer.parseInt(data.get(TAG_PACKED));
		this.count = Integer.parseInt(data.get(TAG_COUNT));
		this.pages = Integer.parseInt(data.get(TAG_PAGES));
	}

	protected String getTagValue(String variable)
	{
		return data.get(variable);
	}

	public String getFace()
	{
		return face;
	}

	public String getFile()
	{
		return file;
	}

	public String getCharset()
	{
		return charset;
	}

	public boolean getBoldFlag()
	{
		return bold;
	}

	public boolean getItalicFlag()
	{
		return italic;
	}

	public boolean getUnicodeFlag()
	{
		return unicode;
	}

	public boolean getSmoothFlag()
	{
		return smooth;
	}

	public boolean getAAFlag()
	{
		return aa;
	}

	public int[] getPaddingArray()
	{
		return padding;
	}

	public int getPaddingTop()
	{
		return paddingTop;
	}

	public int getPaddingLeft()
	{
		return paddingLeft;
	}

	public int getPaddingBottom()
	{
		return paddingBottom;
	}

	public int getPaddingRight()
	{
		return paddingRight;
	}

	public int getSize()
	{
		return size;
	}

	public int getStretchH()
	{
		return stretchH;
	}

	public int[] getSpacing()
	{
		return spacing;
	}

	public int getLineHeight()
	{
		return lineHeight;
	}

	public int getBase()
	{
		return base;
	}

	public int getScaleW()
	{
		return scaleW;
	}

	public int getScaleH()
	{
		return scaleH;
	}

	public int getPages()
	{
		return pages;
	}

	public int getPacked()
	{
		return packed;
	}

	public int getCount()
	{
		return count;
	}
	
	/** 
	 * @param field
	 * @return
	 * -1 if data cannot be converted to boolean
	 * 0 if data is boolean, valid, and {@code false}
	 * 1 if data is boolean, valid, and {@code true}
	 */
	private int isBoolean(String field)
	{
		int intField = Integer.parseInt(field);
		
		return intField == 0 || intField == 1 ? intField : -1; 
	}
}
