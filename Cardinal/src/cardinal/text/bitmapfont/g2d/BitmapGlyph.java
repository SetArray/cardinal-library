package cardinal.text.bitmapfont.g2d;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class BitmapGlyph
{
	public static final int SPACE_ASCII = 32;
	
	private int id; //ASCII Character ID
	private float xTextureCoord; //X texture coord of top-left of glyph in texture atlas
	private float yTextureCoord; //Y texture coord of top-left of glyph in texture atlas
	private float xMaxTextureCoord; //X texture coord of bottom-right of glyph in texture atlas
	private float yMaxTextureCoord; //Y texture coord of bottom-right of glyph in texture atlas
	
	private float xOffset; //Distance of cursor to left edge of glyph quad
	private float yOffset; //Distance of cursor to top of glyph quad
	private float quadWidth; //Width of the glyph's quad
	private float quadHeight; //Height of the glyph's quad
	private float xAdvance; //How many pixels for cursor to move to render next glyph

	public BitmapGlyph(int id, float xTextureCoord, float yTextureCoord, float xTexSize, float yTexSize, float xOffset, float yOffset, float quadWidth, float quadHeight, float xAdvance)
	{
		this.id = id;
		this.xTextureCoord = xTextureCoord;
		this.yTextureCoord = yTextureCoord;
		this.xMaxTextureCoord =  xTextureCoord + xTexSize;
		this.yMaxTextureCoord = yTextureCoord + yTexSize;
		
		//non-tex-coord units
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		this.quadWidth = quadWidth;
		this.quadHeight = quadHeight;
		this.xAdvance = xAdvance;
	}
	
	public BitmapGlyph asSize(float size)
	{
		return new BitmapGlyph(this.id, this.xTextureCoord, this.yTextureCoord, this.xMaxTextureCoord, this.yMaxTextureCoord, this.xOffset * size, this.yOffset * size, this.quadWidth * size, this.quadHeight * size, this.xAdvance * size);
	}

	public int getID()
	{
		return id;
	}

	public float getXTextureCoord()
	{
		return xTextureCoord;
	}

	public float getYTextureCoord()
	{
		return yTextureCoord;
	}

	public float getMaxXTextureCoord()
	{
		return xMaxTextureCoord;
	}

	public float getMaxYTextureCoord()
	{
		return yMaxTextureCoord;
	}

	public float getXOffset()
	{
		return xOffset;
	}

	public float getYOffset()
	{
		return yOffset;
	}

	public float getQuadWidth()
	{
		return quadWidth;
	}

	public float getQuadHeight()
	{
		return quadHeight;
	}
	
	public float getXAdvance()
	{
		return xAdvance;
	}
}
