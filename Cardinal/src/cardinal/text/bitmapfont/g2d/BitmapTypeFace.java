package cardinal.text.bitmapfont.g2d;

import java.util.HashSet;
import java.util.Set;

import cardinal.annotation.Preamble;


@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class BitmapTypeFace
{	
	private BitmapFontMetaData metaData;
	private int bitmap;
	
	private Set<BitmapGlyph> glyphs;
	private Set<Integer> asciiCodes;
	
	private String name;

	public BitmapTypeFace(BitmapFontMetaData controlFile, int bitmap, Set<BitmapGlyph> glyphs)
	{
		this.metaData = controlFile;
		this.bitmap = bitmap;
		this.glyphs = glyphs;
		this.asciiCodes = new HashSet<>();
		this.name = controlFile.getFace();
		
		for(BitmapGlyph c : glyphs) asciiCodes.add(c.getID());
	}
	
	protected BitmapFontMetaData getMetaData()
	{
		return metaData;
	}
	
	protected boolean hasCharacter(int asciiCode)
	{
		return asciiCodes.contains(asciiCode);
	}
	
	protected Set<BitmapGlyph> getGlyphs()
	{		
		return new HashSet<BitmapGlyph>(glyphs);
	}
	
	protected int getTexture()
	{
		return bitmap;
	}
	
	protected String getName()
	{
		return name;
	}
}
