package cardinal.text.vectorfont.g2d;

import java.util.HashMap;
import java.util.Map;

import cardinal.annotation.Description;
import cardinal.annotation.Preamble;

@Preamble
(
		dev="Tyler", 
		created="01-16-17", 
		last_modified="01-16-17"
)
@Description("More advanced/customizable type of Font than a BitmapFont using FontAttributes")
public class VectorFont
{
	private Map<FontAttribute, Object> attributes = new HashMap<FontAttribute, Object>();
	
	public VectorFont()
	{
		
	}
}
