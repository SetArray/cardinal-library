package cardinal.text.vectorfont.g2d;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import cardinal.annotation.Description;
import cardinal.annotation.Preamble;

@Preamble
(
		dev = "Tyler", 
		created = "01-15-17", 
		last_modified = "01-16-17"
		)
@Description("Keys and metadata for Font effects")
public class FontAttribute
{
	private String key;

	private FontAttribute(String key)
	{
		this.key = key;
	}

	public static final FontAttribute SIZE = new FontAttribute("size");
	public static final FontAttribute NAME = new FontAttribute("name");
	public static final FontAttribute WEIGHT = new FontAttribute("weight");
	public static final FontAttribute POSTURE = new FontAttribute("posture");
	public static final FontAttribute CONDENSATION = new FontAttribute("condensation");
	public static final FontAttribute JUSTIICATION = new FontAttribute("justification");
	public static final FontAttribute KERNING = new FontAttribute("kerning");
	public static final FontAttribute UNDERLINE = new FontAttribute("underline");
	public static final FontAttribute STRIKETHROUGH = new FontAttribute("strikethrough");
	public static final FontAttribute OVERLINE = new FontAttribute("overline");
	public static final FontAttribute SCRIPT = new FontAttribute("script");
	public static final FontAttribute SMALLCAPS = new FontAttribute("smallcaps");
	public static final FontAttribute FOREGROUND_RGBA = new FontAttribute("color");
	public static final FontAttribute HIGHLIGHT_RGBA = new FontAttribute("highlight");

	@GroupKey("weight") public static final int REGULAR = 0 << 0;
	@GroupKey("weight") public static final int LIGHT = 0 << 1;
	@GroupKey("weight") public static final int BOLD = 0 << 2;

	@GroupKey("posture") public static final int UPRIGHT = 0 << 0;
	@GroupKey("posture") public static final int OBLIQUE_RIGHT = 0 << 1;
	@GroupKey("posture") public static final int OBLIQUE_LEFT = 0 << 2;

	@GroupKey("condensation") public static final int UNCONDENSED = 0 << 0;
	@GroupKey("condensation") public static final int NARROW = 0 << 1;
	@GroupKey("condensation") public static final int STRETCH = 0 << 2;

	@GroupKey("justification") public static final int ALIGN_CENTER = 0 << 0;
	@GroupKey("justification") public static final int ALIGN_LEFT = 0 << 1;
	@GroupKey("justification") public static final int ALIGN_RIGHT = 0 << 2;

	@GroupKey("kerning") public static final int KERNING_OFF = 0 << 0;
	@GroupKey("kerning") public static final int KERNING_ON = 0 << 1;

	@GroupKey("underline") public static final int UNDERLINE_OFF = 0 << 0;
	@GroupKey("underline") public static final int UNDERLINE_ON = 0 << 1;

	@GroupKey("strikethrough") public static final int STRIKETHROUGH_OFF = 0 << 0;
	@GroupKey("strikethrough") public static final int STRIKETHROUGH_ON = 0 << 1;

	@GroupKey("overline") public static final int OVERLINE_OFF = 0 << 0;
	@GroupKey("overline") public static final int OVERLINE_ON = 0 << 1;

	@GroupKey("script") public static final int PLAINSCRIPT = 0 << 0;
	@GroupKey("script") public static final int SUBSCRIPT = 0 << 1;
	@GroupKey("script") public static final int SUPERSCRIPT = 0 << 1;

	@GroupKey("smallcaps") public static final int SMALLCAPS_OFF = 0 << 0;
	@GroupKey("smallcaps") public static final int SMALLCAPS_ON = 0 << 1;

	@GroupKey("color") public static final int COLOR_BLACK = -16777216;
	@GroupKey("color") public static final int COLOR_WHITE = -1;
	@GroupKey("color") public static final int COLOR_GREEN = -16711936;
	@GroupKey("color") public static final int COLOR_BLUE = -16776961;
	@GroupKey("color") public static final int COLOR_PURPLE = -65281;
	@GroupKey("color") public static final int COLOR_YELLOW = -256;
	@GroupKey("color") public static final int COLOR_ORANGE = -14336;
	@GroupKey("color") public static final int COLOR_RED = -65536;
}

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Preamble(dev = "Tyler", created = "01-15-17", last_modified = "01-16-17")
@Description("Used to connect FontAttribute constants to a FontAttribute")
@interface GroupKey
{
	String value();
}
