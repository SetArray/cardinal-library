package cardinal.text.g2d;

import cardinal.annotation.Preamble;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Caret
{
	boolean visible;
	boolean blink;
	int caretPosition;
	int markPosition;
	
	public void moveDot(int position)
	{
		this.caretPosition = position;
	}
	
	public void setDot(int position)
	{
		this.caretPosition = position;
		this.markPosition = position;
	}
	
	public int getDot()
	{
		return caretPosition;
	}
	
	public int getMark()
	{
		return markPosition;
	}
	
	public void blink(boolean blink)
	{
		this.blink = blink;
	}
	
	public boolean isBlinking()
	{
		return blink;
	}
	
	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}
	
	public boolean isVisible()
	{
		return visible;
	}
}
