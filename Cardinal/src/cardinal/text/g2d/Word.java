package cardinal.text.g2d;

import cardinal.annotation.Preamble;


@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Word
{
	private String word;
	private char[] characters;
	
	public Word(char[] characters)
	{
		word = String.copyValueOf(characters);
		this.characters = characters;
	}
	
	public char getCharacter(int index)
	{
		return characters[index];
	}	

	public char[] getCharacters()
	{
		return characters;
	}
	
	@Override
	public String toString()
	{
		return word;
	}
	
	@Override
	public boolean equals(Object object)
	{
		return (object instanceof Word && word.equals(object.toString()));
	}
}