package cardinal.text.g2d;

import java.util.ArrayList;
import java.util.List;

import cardinal.annotation.Preamble;
import cardinal.text.bitmapfont.g2d.BitmapFontMetaData;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Line
{
	public static float LINE_HEIGHT = 0.03f;
	
	private List<Word> words = new ArrayList<Word>();
	
	private float fontSize;
	private float lineHeight;
	private float spaceWidth;
	private float maxLength;
	private float currentLineLength;

	protected Line(BitmapFontMetaData metaData, float fontSize, float maxLength)
	{
		this.fontSize = fontSize;
		this.lineHeight = LINE_HEIGHT / (float) (metaData.getLineHeight() - metaData.getPaddingTop());
		this.spaceWidth = 0;//metaData.getSpaceWidth() * fontSize;
		this.maxLength = maxLength;
		this.currentLineLength = 0.0f;
	}

	protected boolean attemptToAddWord(Word word)
	{
		float additionalLength = 0;//word.getWordWidth(fontSize);
		// += !words.isEmpty() ? spaceWidth : 0.0f;
		
		if(currentLineLength + additionalLength <= maxLength)
		{
			words.add(word);
			currentLineLength += additionalLength;
			return true;
		}
		else
			return false;
	}

	protected List<Word> getWords()
	{
		return words;
	}
	
	protected float getMaxLength()
	{
		return maxLength;
	}

	protected float getLineHeight()
	{
		return lineHeight;
	}
	
	protected float getLineLength()
	{
		return currentLineLength;
	}
	
	protected float getSpaceWidth()
	{
		return spaceWidth;
	}
	
	protected float getFontSize()
	{
		return fontSize;
	}
}
