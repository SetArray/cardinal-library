package cardinal.text.g2d;

import java.util.ArrayList;
import java.util.List;

import cardinal.text.bitmapfont.g2d.BitmapFont;

public class TextBox
{
	/*
	 * Type Family: 
	 * Type Face: 
	 * Type Style: 
	 * Glyph: 
	 */
	
	private int actualWidth, actualHeight;

	private boolean lineWrap;
	private boolean wrapStyleWord;

	private BitmapFont font;

	private Caret caret;
	
	private List<Word> words = new ArrayList<>();
	private List<Word[]> lines = new ArrayList<>();

	public TextBox(int width, int height, BitmapFont font)
	{
		this(width, height, font, "");
	}
	public TextBox(int width, int height, BitmapFont font, String text)
	{
		this.actualWidth = width;
		this.actualHeight = height;
		
		this.lineWrap = false;
		this.wrapStyleWord = false;
		this.font = null;
		
		this.caret = new Caret();
		setText(text);
	}
	
	public void setText(String text)
	{
		if(text == null || text.equals(""))
		{
			clearText();
			return;
		}
		
		append(text);
	}
	
	public void append(String text)
	{
		words.addAll(getWordsInString(text));
	}
	
	public void insert(String str, int index)
	{
		words.addAll(index, getWordsInString(str));
	}
	
	public void clearText()
	{
		words.clear();
		lines.clear();
	}
	
	public Word[] debugGetWords()
	{
		return words.toArray(new Word[0]);
	}
	
	public BitmapFont debugGetFont()
	{
		return font;
	}
	
	public void setBorder(int width, int height)
	{
		this.actualWidth = width;
		this.actualHeight = height;
	}
	
	public void setLineWrap(boolean wrap)
	{
		this.lineWrap = wrap;
	}
	
	public void setWrapStyleWord(boolean wrapWord)
	{
		this.wrapStyleWord = wrapWord;
	}
	
	public void setFont(BitmapFont font)
	{
		this.font = font;
	}
	
	public boolean getLineWrap()
	{
		return lineWrap;
	}
	
	public boolean getWrapStyleWord()
	{
		return wrapStyleWord;
	}
	
	private List<Word> getWordsInString(String str)
	{
		String[] wordStrings = str.split(" ");
		List<Word> stringWords = new ArrayList<>();
		
		for(String s : wordStrings)
			stringWords.add(new Word(s.toCharArray()));
		
		return stringWords;
	}
}
