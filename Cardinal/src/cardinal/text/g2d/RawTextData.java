package cardinal.text.g2d;

import cardinal.annotation.Preamble;
import cardinal.text.bitmapfont.g2d.BitmapTypeFace;


@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class RawTextData
{
	private BitmapTypeFace fontType;
	
	private String text;
	private float fontSize;

	private float maxLineSize;
	private int lineCount;

	private boolean centerText;
	
	public RawTextData(BitmapTypeFace fontType, String text, float fontSize, float maxLineLength, boolean centered)
	{
		this.fontType = fontType;
		this.text = text;
		this.fontSize = fontSize;
		this.maxLineSize = maxLineLength;
		this.centerText = centered;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	
	public String getText()
	{
		return text;
	}
	
	public void setFontSize(float fontSize)
	{
		this.fontSize = fontSize;
	}
	
	public float getFontSize()
	{
		return fontSize;
	}
	
	public void setFontType(BitmapTypeFace fontType)
	{
		this.fontType = fontType;
	}
	
	public BitmapTypeFace getFontType()
	{
		return fontType;
	}
	
	public void setMaxLineSize(float maxLineSize)
	{
		this.maxLineSize = maxLineSize;
	}
	
	public float getMaxLineSize()
	{
		return maxLineSize;
	}
	
	public void setLineCount(int numLines)
	{
		this.lineCount = numLines;
	}
	
	public int getLineCount()
	{
		return lineCount;
	}
	
	public void setCentered(boolean isCentered)
	{
		this.centerText = isCentered;
	}
	
	public boolean isCentered()
	{
		return centerText;
	}
}
