package test.game.blueprints;

import org.lwjgl.util.vector.Vector3f;

import cardinal.annotation.Preamble;
import cardinal.componentArch.Blueprint;
import cardinal.componentArch.g3d.RenderComponent;
import cardinal.componentArch.g3d.RenderPropertiesComponent;
import cardinal.componentArch.g3d.TexturedModelComponent;
import cardinal.componentArch.g3d.TransformComponent;
import cardinal.graphics.model.g3d.TexturedModel;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class StaticBlueprint extends Blueprint
{
	public StaticBlueprint(TexturedModel model)
	{
		this(new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), model);
	}
	
	public StaticBlueprint(Vector3f position, TexturedModel model)
	{
		this(position, new Vector3f(0.0f, 0.0f, 0.0f), model);
	}
	
	public StaticBlueprint(Vector3f position, Vector3f rotation, TexturedModel model)
	{
		super(getStaticID());
		
		int transform = addComponent(new TransformComponent(position, rotation));
		int texturedModel = addComponent(new TexturedModelComponent(model));
		int renderProperties = addComponent(new RenderPropertiesComponent(true));
		int render = addComponent(new RenderComponent(model, model.getModelTextures(), (RenderPropertiesComponent) components.get(renderProperties)));
	}
	
	public RenderPropertiesComponent getRenderProperties()
	{
		return (RenderPropertiesComponent) getComponent(RenderPropertiesComponent.getStaticID());
	}
	
	public static String getStaticID()
	{
		return "StaticBlueprint";
	}
}
