package test.game;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import test.game.blueprints.StaticBlueprint;
import test.game.console.CommandSetRenderMode;
import test.game.console.TestConsole;
import test.main.Preloads;
import cardinal.annotation.CardinalFont;
import cardinal.annotation.Preamble;
import cardinal.componentArch.GameObject;
import cardinal.componentArch.events.TransformEvent;
import cardinal.componentArch.g3d.TransformComponent;
import cardinal.controls.Controller;
import cardinal.graphics.RenderSystem;
import cardinal.graphics.g3d.Camera;
import cardinal.shader.Shaders;
import cardinal.text.bitmapfont.g2d.BitmapFont;
import cardinal.text.g2d.TextBox;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Game
{
	private TestConsole console;
	private Camera camera;

	private RenderSystem renderer;

	private List<GameObject> entities = new ArrayList<GameObject>();
	private List<GameObject> waterTiles = new ArrayList<GameObject>();
	private List<GameObject> terrains = new ArrayList<GameObject>();
	private List<GameObject> guis = new ArrayList<GameObject>();

	public GameObject testEntity, terrain, textVersion, waterTile, skybox;

	public Game()
	{
		renderer = new RenderSystem(60.0f, 0.01f, 10000.0f);
		
		console = new TestConsole();
		console.registerCommand(CommandSetRenderMode.getInstance());

		camera = new Camera();
		camera.setRotationMinima(new Vector3f(-70.0f, -361.0f, -361.0f));
		camera.setRotationMaxima(new Vector3f(70.0f, 361.0f, 361.0f));
		camera.restrictRotation(true);

		StaticBlueprint terrainBlueprint = new StaticBlueprint(Preloads.HEIGHTMAP0);
		terrainBlueprint.getRenderProperties().setClampTextureOnEdges(true);
		terrain = new GameObject(terrainBlueprint);
		
		testEntity = new GameObject(new StaticBlueprint(new Vector3f(50.0f, Preloads.HEIGHTMAP0.getHeightAtWorldPosition(new Vector3f(50.0f, 0.0f, 50.0f)), 50.0f), Preloads.PLAYER_TEXTURED_MODEL));
		
		/*
		Blueprint textBlueprint = new Blueprint("TextBlueprint");
		TextComponent textComponent = new TextComponent(Preloads.VERSION_TEXT_MODEL);
		textBlueprint.addComponent(new TransformComponent(new Vector2f(0.0f, 0.0f)));
		textBlueprint.addComponent(textComponent);
		int renderProperties = textBlueprint.addComponent(new RenderPropertiesComponent(false));
		textBlueprint.addComponent(new RenderComponent(textComponent.getTextModel(), textComponent.getTextModel().getModelTextures(), (RenderPropertiesComponent) textBlueprint.getComponent(renderProperties)));
		
		textVersion = new GameObject(textBlueprint);
		*/
		
		TextBox textBox = new TextBox(200, 200, Preloads.CALIBRI_BITMAPFONT, "This is a text box!");
		
		//textBox.setFont(new Font(Preloads.CALIBRI_FONT_TYPEFACE, Font.PLAIN, 1.0f));
		/*for(Word word : textBox.debugGetWords())
		{
			for(char c : word.getCharacters())
			{
				if(word.equals(textBox.debugGetWords()[0]))
				{
					float[] vertices = textBox.debugGetFont().debugGetVertexData(c)[0];
				}
			}
		}
		*/
		waterTile = new GameObject(new StaticBlueprint(new Vector3f(110.0f, 12.0f, 20.0f), Preloads.WATER_TILE_MODEL));
		
		skybox = new GameObject(new StaticBlueprint(Preloads.SKYBOX_MODEL));

		terrains.add(terrain);
		entities.add(testEntity);
		waterTiles.add(waterTile);
		//guis.add(textVersion);
	}

	public void update()
	{
		if(Controller.isKeyPressed(Controller.KEY_1))
			console.processCommandLine("/setRenderMode 0");
		if(Controller.isKeyPressed(Controller.KEY_2))
			console.processCommandLine("/setRenderMode 1 2 4");

		console.update();
		camera.update();
		skybox.update();
		
		if(Preloads.HEIGHTMAP0.isWorldPositionInBounds(camera.getPosition()))
			camera.getPosition().y = Preloads.HEIGHTMAP0.getHeightAtWorldPosition(camera.getPosition()) + 15.0f;
		skybox.fireEvent(new TransformEvent(camera.getPosition(), new Vector3f(0, 0, 0), false));
		
		for(GameObject object : terrains)
			object.update();
		for(GameObject object : waterTiles)
			object.update();
		for(GameObject object : entities)
			object.update();
		for(GameObject object : guis)
			object.update();
	}

	public void lateUpdate()
	{
		skybox.lateUpdate();
		
		for(GameObject object : terrains)
			object.lateUpdate();
		for(GameObject object : waterTiles)
			object.lateUpdate();
		for(GameObject object : entities)
			object.lateUpdate();
		for(GameObject object : guis)
			object.update();
	}

	public void render()
	{
		Preloads.WATER_BUFFERS.bindReflectionFrameBuffer();
		{
			float distance = 2.0f * (camera.getPosition().y - ((TransformComponent)waterTile.getComponent(0)).getY());
			
			camera.getPosition().y -= distance;
			camera.getRotation().x *= -1.0f;
			render3D(new Vector4f(0.0f, 1.0f, 0.0f, -((TransformComponent)waterTile.getComponent(0)).getY()));
			renderer.render(Shaders.STATIC_SHADER, skybox, camera, new Vector4f(0.0f, 1.0f, 0.0f, -((TransformComponent)waterTile.getComponent(0)).getY()));
			camera.getPosition().y += distance;
			camera.getRotation().x *= -1.0f;
		}
		
		Preloads.WATER_BUFFERS.bindRefractionFrameBuffer();
		{
			render3D(new Vector4f(0.0f, -1.0f, 0.0f, ((TransformComponent)waterTile.getComponent(0)).getY()));
		}
			
		Preloads.WATER_BUFFERS.unbindCurrentFrameBuffer();
		{
			render3D(new Vector4f(0.0f, -1.0f, 0.0f, 10000.0f));
			renderer.render(Shaders.WATER_SHADER, waterTiles, camera, camera.getPosition());
			renderer.render(Shaders.STATIC_SHADER, skybox, camera);

			render2D();
		}
	}

	private void render3D(Vector4f clipPlane)
	{
		RenderSystem.enableGL3D();
		RenderSystem.clearColorBufferBit();
		RenderSystem.clearDepthBufferBit();
		
		renderer.render(Shaders.TERRAIN_SHADER, terrains, camera, clipPlane);
		renderer.render(Shaders.STATIC_SHADER, entities, camera, clipPlane);
	}

	private void render2D()
	{
		RenderSystem.enableGL2D();

		 renderer.render(Shaders.TEXT_SHADER, guis, camera);
	}
	
	/*
	    GL11.glMatrixMode(GL11.GL_PROJECTION);
	    GL11.glLoadIdentity();
	    GL11.glOrtho(0, Display.getWidth(), 0, Display.getHeight(), -1, 1);
	    GL11.glMatrixMode(GL11.GL_MODELVIEW);
	    GL11.glLoadIdentity();

		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, Preloads.WATER_BUFFERS.getReflectionTexture());
		GL11.glBegin(GL11.GL_QUADS);
		{
			GL11.glTexCoord2d(0.0, 0.0);
			GL11.glVertex2d(0.0, 0.0);

			GL11.glTexCoord2d(0.0, 1.0);
			GL11.glVertex2d(0.0, Window.getHeight()/4.0f);

			GL11.glTexCoord2d(1.0, 1.0);
			GL11.glVertex2d(Window.getWidth() / 4.0f, Window.getHeight()/4.0f);

			GL11.glTexCoord2d(1.0, 0.0);
			GL11.glVertex2d(Window.getWidth() / 4.0f, 0.0);
		}
		GL11.glEnd();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	*/
}
