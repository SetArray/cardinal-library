package test.game.console;

import org.lwjgl.opengl.GL11;

import cardinal.annotation.Preamble;
import cardinal.console.Command;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class CommandSetRenderMode extends Command
{	
	private static final CommandSetRenderMode instance = new CommandSetRenderMode();
	
	public static CommandSetRenderMode getInstance()
	{
		return instance;
	}

	@Override
	public String getCommandName()
	{
		return "setRenderMode";
	}

	@Override
	public String getCommandUsage()
	{
		return "Sets the render mode.";
	}

	@Override
	public void processCommand(String[] args)
	{
		int renderMode = Integer.parseInt(args[0]);
		if(renderMode == 0)
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
		if(renderMode == 1)
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
	}
}
