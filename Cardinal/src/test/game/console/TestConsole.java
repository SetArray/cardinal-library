package test.game.console;

import cardinal.annotation.Preamble;
import cardinal.console.Console;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class TestConsole extends Console
{
	public TestConsole()
	{
		super();
	}

	@Override
	public void update()
	{
		if(isOpen())
		{
			//TODO
		}
	}

	@Override
	public void render()
	{
		if(isOpen())
		{
			
		}
	}
}
