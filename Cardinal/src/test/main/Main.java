package test.main;

import test.game.Game;
import cardinal.annotation.Preamble;
import cardinal.controls.Controller;
import cardinal.graphics.RenderSystem;
import cardinal.window.Window;

@Preamble(dev="Tyler", created="?", last_modified="01-15-17")
public class Main
{
	private static Main instance;
	private Game game;
	
	public void start()
	{
		preInit();
		gameLoop();
	}
	
	public void preInit()
	{		
		Window.createWindow(1200, 700, "Cardinal Library");
		Window.setCloseOnKeyPress(Controller.KEY_ESCAPE, true);
		Window.setSync(0);
		
		RenderSystem.enableGL();
		
		Controller.grabMouse(true);
		Preloads.loadModels();
		
		game = new Game();
	}
	
	public void gameLoop()
	{
		while(!Window.isCloseRequested())
		{
			game.update();
			game.lateUpdate();
			game.render();
			
			Window.update();
			Window.setTitle("Game | FPS: " + Window.getFPS() + " Delta: " + Window.getDelta());
		}
		
		destroy();
	}
	
	public void destroy()
	{
		Preloads.cleanUp();
		Window.destroy();
	}
	
	public static Main getInstance()
	{
		return instance;
	}
	
	public Game getGame()
	{
		return game;
	}
	
	public static void main(String[] args)
	{
		instance = new Main();
		instance.start();
	}
}
