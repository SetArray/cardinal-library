package test.main;

import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import cardinal.annotation.CardinalFont;
import cardinal.annotation.Description;
import cardinal.annotation.Preamble;
import cardinal.graphics.g3d.ModelTexture;
import cardinal.graphics.g3d.WaterFrameBuffers;
import cardinal.graphics.model.g2d.ModelLoader2D;
import cardinal.graphics.model.g2d.TextModel;
import cardinal.graphics.model.g3d.HeightmapModel;
import cardinal.graphics.model.g3d.ModelLoader3D;
import cardinal.graphics.model.g3d.RawModel;
import cardinal.graphics.model.g3d.TexturedModel;
import cardinal.text.bitmapfont.g2d.BitmapFont;
import cardinal.text.bitmapfont.g2d.BitmapTypeFace;
import cardinal.text.g2d.RawTextData;
import cardinal.util.FileUtil;

@Preamble
(
		dev="Tyler", 
		created="?", 
		last_modified="01-16-17"
)
@Description("Collection of variables for test/debugging")
public class Preloads
{
	public static WaterFrameBuffers WATER_BUFFERS;

	public static int TEXTURE_PLAYER;

	public static int TEXTURE_HEIGHTMAP0_DEBUG, TEXTURE_HEIGHTMAP0_DEBUG1;

	public static int TEXTURE_HEIGHTMAP0_GRASS;
	public static int TEXTURE_HEIGHTMAP0_DIRT;
	public static int TEXTURE_HEIGHTMAP0_ROCK;
	public static int TEXTURE_HEIGHTMAP0_SAND;
	public static int TEXTURE_BLENDMAP0;

	public static int TEXTURE_WATER_DUDV_RIPPLE;

	public static int TEXTURE_SKYBOX;

	public static int[] HEIGHTMAP0_TEXTURES;
	public static ModelTexture[] HEIGHTMAP0_TEXTURE_PACK;

	public static ModelLoader2D loader2D;
	public static ModelLoader3D loader3D;

	public static HeightmapModel HEIGHTMAP0;
	public static RawModel PLAYER_RAW_MODEL, WATER_TILE_RAW_MODEL, SKYBOX_RAW_MODEL;
	public static TexturedModel PLAYER_TEXTURED_MODEL, WATER_TILE_MODEL, SKYBOX_MODEL;

	public static BitmapTypeFace CALIBRI_BITMAPFONT_TF;
	public static RawTextData VERSION_RAW_TEXT_DATA;
	public static TextModel VERSION_TEXT_MODEL;

	@CardinalFont(
			type=Preloads.class, 
			field="CALIBRI_CARDINAL_FONT", 
			size=12, 
			weight=1.0f, 
			posture=1, 
			condensation=1, 
			justification=0.5f, 
			kerning=false, 
			underline=false, 
			strikethrough=false, 
			overline=true
			)
	public static BitmapFont CALIBRI_BITMAPFONT;
	
	public static void loadModels()
	{
		try
		{
			loader2D = new ModelLoader2D();
			loader3D = new ModelLoader3D();

			WATER_BUFFERS = new WaterFrameBuffers();

			TEXTURE_PLAYER = FileUtil.loadResourceAsTexture("entities/player.png");
			PLAYER_RAW_MODEL = FileUtil.loadResourceAsRawModel("entities/player.obj", loader3D);
			PLAYER_TEXTURED_MODEL = new TexturedModel(PLAYER_RAW_MODEL, new ModelTexture(TEXTURE_PLAYER));

			TEXTURE_HEIGHTMAP0_DEBUG = FileUtil.loadResourceAsTexture("terrain/256x256/debug.png");
			TEXTURE_HEIGHTMAP0_DEBUG1 = FileUtil.loadResourceAsTexture("terrain/256x256/debug1.png");
			TEXTURE_HEIGHTMAP0_GRASS = FileUtil.loadResourceAsTexture("terrain/16x16/grass0.png");
			TEXTURE_HEIGHTMAP0_DIRT = FileUtil.loadResourceAsTexture("terrain/16x16/dirt0.png");
			TEXTURE_HEIGHTMAP0_ROCK = FileUtil.loadResourceAsTexture("terrain/16x16/rock0.png");
			TEXTURE_HEIGHTMAP0_SAND = FileUtil.loadResourceAsTexture("terrain/16x16/sand0.png");
			TEXTURE_BLENDMAP0 = FileUtil.loadResourceAsTexture("terrain/256x256/blendmap0.png");
			TEXTURE_WATER_DUDV_RIPPLE = FileUtil.loadResourceAsTexture("terrain/waterDUDV.png");
			
			HEIGHTMAP0_TEXTURE_PACK = new ModelTexture[]
			{ 
					new ModelTexture(TEXTURE_HEIGHTMAP0_GRASS), 
					new ModelTexture(TEXTURE_HEIGHTMAP0_ROCK), 
					new ModelTexture(TEXTURE_HEIGHTMAP0_SAND),
					new ModelTexture(TEXTURE_HEIGHTMAP0_DIRT), 
					new ModelTexture(TEXTURE_BLENDMAP0)
			};

			HEIGHTMAP0 = FileUtil.loadResourceAsHeightmapModel("terrain/heightmap0.png", new Vector3f(10.0f, 1f, 10.0f),
					HEIGHTMAP0_TEXTURE_PACK, loader3D);

			CALIBRI_BITMAPFONT_TF = FileUtil.loadTypeFace("font/calibri.fnt", "font/calibri.png");
			CALIBRI_BITMAPFONT = new BitmapFont(CALIBRI_BITMAPFONT_TF, 0);
			
			Vector3f v0 = new Vector3f(0.0f, 0.0f, 300.0f);
			Vector3f v1 = new Vector3f(0.0f, 0.0f, 0.0f);
			Vector3f v2 = new Vector3f(290.0f, 0.0f, 300.0f);
			Vector3f v3 = new Vector3f(290.0f, 0.0f, 0.0f);

			WATER_TILE_RAW_MODEL = loader3D.createFlatRawModel(v0, v1, v2, v3);
			WATER_TILE_MODEL = new TexturedModel(WATER_TILE_RAW_MODEL, new ModelTexture(WATER_BUFFERS.getReflectionTexture()), new ModelTexture(WATER_BUFFERS.getRefractionTexture()), new ModelTexture(WATER_BUFFERS.getRefractionDepthTexture()), new ModelTexture(TEXTURE_WATER_DUDV_RIPPLE));

			TEXTURE_SKYBOX = FileUtil.loadResourceAsTexture("terrain/skybox.png");
			SKYBOX_RAW_MODEL = FileUtil.loadResourceAsRawModel("terrain/skybox.obj", loader3D);
			SKYBOX_MODEL = new TexturedModel(SKYBOX_RAW_MODEL, new ModelTexture(TEXTURE_SKYBOX));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void cleanUp()
	{
		WATER_BUFFERS.cleanUp();
		
		loader3D.cleanUp();
	}
}
