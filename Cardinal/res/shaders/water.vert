in vec3 vertices;
in vec2 texCoords;
in vec3 normals;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform vec3 cameraPosition;

out vec4 clipSpace;
out vec3 waterToEyeVector;
out vec2 passTexCoords;

void main()
{	
	vec4 worldPosition = modelMatrix * vec4(vertices, 1.0);
	clipSpace = projectionMatrix * viewMatrix * worldPosition;
	
    waterToEyeVector = cameraPosition - worldPosition.xyz;
    passTexCoords = (texCoords.xy / 2.0 + 0.5) * 6.0;
    
    gl_Position = clipSpace;
}