in vec3 vertices;
in vec2 texCoords;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform vec4 clipPlane;

out vec2 passTexCoords;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertices, 1.0f);
	passTexCoords = texCoords;
	
	gl_ClipDistance[0] = dot(modelMatrix * vec4(vertices, 1.0f), clipPlane);
}