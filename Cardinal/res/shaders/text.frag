in vec2 passTexCoords;

uniform vec4 fontColor;
uniform sampler2D fontAtlas;

void main()
{
	vec4 outColor = vec4(fontColor.xyz, texture(fontAtlas, passTexCoords).a);
	
	if(outColor.a > 0.0f)
		gl_FragColor = outColor;
	else
		discard;
}