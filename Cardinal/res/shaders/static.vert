in vec3 vertices;
in vec2 texCoords;
in vec3 normals;

uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform int numberOfRows;
uniform vec2 indexOffset;

uniform vec4 clipPlane;

out vec2 passTexCoords;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertices, 1.0f);
	passTexCoords = (texCoords / numberOfRows) + indexOffset;
	
	gl_ClipDistance[0] = dot(modelMatrix * vec4(vertices, 1.0f), clipPlane);
}