in vec2 vertices;
in vec2 texCoords;

uniform mat4 orthogonalProjectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform vec2 position;

out vec2 passTexCoords;

void main()
{
	gl_Position = vec4(position + vertices, 0.0f, 1.0f);
	passTexCoords = texCoords;
}