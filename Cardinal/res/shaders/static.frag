in vec2 passTexCoords;

uniform float opacity;
uniform sampler2D texSampler;

void main()
{
	vec4 color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	
	vec4 textureColor = texture(texSampler, passTexCoords);
	textureColor.w *= opacity;
	
	if(textureColor.w == 0)
		discard;
	else
		color *= textureColor;

	gl_FragColor = color;
}