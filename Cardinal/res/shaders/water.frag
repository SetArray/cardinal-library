in vec4 clipSpace;
in vec3 waterToEyeVector;
in vec2 passTexCoords;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D refractionDepthTexture;
uniform sampler2D rippleDUDVTexture;

uniform float moveFactor;

const float rippleStrength = 0.02f;
const float reflectivity = 0.45f;

void main()
{
	vec2 deviceSpace = (clipSpace.xy / clipSpace.w) / 2.0 + 0.5;
	
	vec2 undistortedRreflectionTexCoords  = vec2(deviceSpace.x, -deviceSpace.y);
	vec2 undistortedRefractionTexCoords = vec2(deviceSpace.x, deviceSpace.y);
	vec2 reflectionTexCoords =undistortedRreflectionTexCoords;
	vec2 refractionTexCoords = undistortedRefractionTexCoords;
	
	float near = 0.01f;
	float far = 10000.0f;
	float depth = texture(refractionDepthTexture, undistortedRefractionTexCoords).r;
	float floorDistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));
	
	depth = gl_FragCoord.z;
	float waterDistance = 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));
	float waterDepth = floorDistance - waterDistance;
	
	vec2 distortedTexCoords = texture(rippleDUDVTexture, vec2(passTexCoords.x + moveFactor, passTexCoords.y)).rg * 0.1;
	distortedTexCoords = passTexCoords + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
	vec2 totalDistortion = (texture(rippleDUDVTexture, distortedTexCoords).rg * 2.0 - 1.0)  * rippleStrength * clamp(waterDepth / 20.0, 0.0, 1.0);
	
	reflectionTexCoords.x += totalDistortion.x;		
	reflectionTexCoords.y -= totalDistortion.y;	
	refractionTexCoords += totalDistortion;
	
	//Fixes visual artifacts in Clip Space
	reflectionTexCoords.x = clamp(reflectionTexCoords.x, 0.001, 0.999);
	reflectionTexCoords.y = clamp(reflectionTexCoords.y, -0.999, -0.001);
	refractionTexCoords = clamp(refractionTexCoords, 0.001, 0.999);
	
	vec4 reflectionColor = texture(reflectionTexture, reflectionTexCoords);
	vec4 refractionColor = texture(refractionTexture, refractionTexCoords);
	if(refractionColor.xyz == vec3(0, 0, 0))
	{
		reflectionColor = texture(reflectionTexture, undistortedRreflectionTexCoords);
		refractionColor = texture(refractionTexture, undistortedRefractionTexCoords);
	}
	
	vec3 viewVector = normalize(waterToEyeVector);
	float refractiveFactor = dot(viewVector, vec3(0.0, 1.0, 0.0));
	refractiveFactor = pow(refractiveFactor, reflectivity);
	refractiveFactor = clamp(refractiveFactor, 0.0, 1.0);
	

	gl_FragColor = mix(mix(reflectionColor, refractionColor, refractiveFactor), vec4(0.0, 0.3, 0.5, 1.0), 0.25);
	gl_FragColor.a = clamp(waterDepth / 10.0, 0.0, 1.0);
}