in vec2 passTexCoords;

uniform sampler2D backgroundTexture;
uniform sampler2D rTexture;
uniform sampler2D gTexture;
uniform sampler2D bTexture;
uniform sampler2D blendMapTexture;

void main()
{
	vec2 tiledTexCoords = passTexCoords * 127.0f;
	
	vec4 blendColor = texture(blendMapTexture, passTexCoords);
	
	float backgroundTextureAmount = 1 - (blendColor.r + blendColor.g + blendColor.b);
	
	vec4 backgroundColor = texture(backgroundTexture, tiledTexCoords) * backgroundTextureAmount;
	vec4 rColor = texture(rTexture, tiledTexCoords) * blendColor.r;
	vec4 gColor = texture(gTexture, tiledTexCoords) * blendColor.g;
	vec4 bColor = texture(bTexture, tiledTexCoords) * blendColor.b;
	
	vec4 totalColor = backgroundColor + rColor + gColor + bColor;
	
	gl_FragColor = totalColor;
}