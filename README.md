### Cardinal 3D/2D Game Library ###

Cardinal is a Java-written gaming library designed using OpenGL in conjunction with LWJGL. This library contains a plethora of useful tools and utilities for the creation of 2-D and 3-D games.
Cardinal's API utilizes a **Component-Based Architecture** for ease of use. Tools include (but are not limited to):

* Advanced Rendering (Reflection/Refraction Support!)
* Shadow/Bumpmapping
* Terrain/Texture Blending
* Math/Vector Utils
* File I/O Utils
* Font Rendering
* Model Blueprints
* Shader Support
* Custom Annotation
* And more...
* V 0.5.0

### Contribution guidelines ###

* Ensure all code is up-to-date with the repo; be certain code functions properly prior to upshing to the Git.
* Please format commits appropriately to accurately describe the purpose of the commit and anything accomplished/fixed by the pushed code.

### Who do I talk to? ###

* Emily Smith (emily9716@outlook.com)